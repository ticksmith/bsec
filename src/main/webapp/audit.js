/*
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*jslint white: true, browser: true, devel: true, todo: true, vars: true, nomen: true, regexp: true, sloppy: true, indent: 4, maxerr: 50 */
/*global App*/
/*global butor*/
/*global AJAX*/
/*global moment*/
/*global $*/
//# sourceURL=bsec.audit.js
butor.sec.audit = butor.sec.audit || {
	auditFte : ["id", "displayName", "firmName"],
	serviceCallFte : ["user", "sessionId", "namespace", "service", "success", "duration", "payload"]
};
butor.sec.audit.AuditAjax = butor.sec.audit.AuditAjax || function () {
	return {
		listAudit: function (args, handler) {
			return AJAX.call('/bsec/audit.ajax', {
				streaming: false,
				service: 'listAudit'
			}, [args], handler);
		},
		listServiceCall: function (args, handler) {
			return AJAX.call('/bsec/audit.ajax', {
				streaming: false,
				service: 'listServiceCall'
			}, [args], handler);
		},
		exportServiceCall: function (args, handler) {
			return AJAX.call('/bsec/auditBin.ajax', {
				streaming: false,
				service: 'exportServiceCall',
				download: true
			}, [args], handler);
		}
	};
}();
butor.sec.audit.AuditList = butor.sec.audit.AuditList || butor.Class.define({
	_jqe: null,
	_resultTable: null,
	_dateTo: null,
	_dateFrom: null,
	tr: butor.sec.tr,
	construct: function (jqe) {
		this._jqe = jqe;
		this._jqe.find('#goBtn').click($.proxy(this._go, this));
		this._jqe.find('#resetBtn').click($.proxy(this._reset, this));
		this._resultTable = this._jqe.find('#resultTable');
		this._dateFrom = new butor.DatePicker(this._jqe.find('.dateFromGroup'), { 'errorMsg': this.tr('Date from is not valid!') });
		this._dateTo = new butor.DatePicker(this._jqe.find('.dateToGroup'), { 'errorMsg': this.tr('Date to is not valid!') });
		this._jqe.keypress($.proxy(function (e) {
			if (e.keyCode === 13) {
				this._go();
				return false;
			}
		}, this));
		this._resultTable.tablesorter();
		this._reset();
	},
	_go: function () {
		var reqId, criteria = this._getCriteria();
		if (!this._validateCriteria(criteria)) {
			return;
		}
		App.mask(this.tr('Loading'));
		reqId = butor.sec.audit.AuditAjax.listAudit(criteria, $.proxy(function (result) {
			var i, row, l, tbody = this._resultTable.find('tbody'), res;
			this._clearResult();
			App.unmask();
			if (!result || result.hasError || !result.data || reqId !== result.reqId) {
				return;
			}
			for (i = 0, l = result.data.length; i < l; i = i + 1) {
				res = result.data[i];
				
				//IMPORTANT escape fields to use with .html() or set them with .text()
				//butor.Utils.escape(res, butor.sec.audit.auditFte);
				
				row = $('<tr>');
				row.append($('<td>').text(i + 1));
				row.append($('<td>').text(res.id));
				row.append($('<td>').text(res.displayName));
				row.append($('<td>').text(res.firmName));
				row.append($('<td>').text(butor.Utils.formatDateTime(res.lastLoginDate)));
				var attributes = res.attributesMap;
				if (attributes) {
					if (butor.Utils.isEmpty(attributes.reverseLookup)) {
						row.append($('<td>').text(attributes.ip));
					} else {
						row.append($('<td>').text(attributes.ip + ' / ' + attributes.reverseLookup));
					}
					row.append($('<td>').text(attributes.domain));
					row.append($('<td>').text(attributes.userAgent));
				}
				tbody.append(row);
			}
			this._resultTable.trigger('update');
		}, this));
	},
	_validateCriteria: function (criteria) {
		var valid = false;
		if (criteria) {
			if (butor.Utils.isEmpty(criteria.dateFrom)) {
				this._jqe.find('.dateFromGroup input').focus();
				App.msgError(this.tr('Date from is mandatory'));
				return false;
			}
			if (butor.Utils.isEmpty(criteria.dateTo)) {
				this._jqe.find('.dateToGroup input').focus();
				App.msgError(this.tr('Date to is mandatory'));
				return false;
			}
			valid = true;
		}
		return valid;
	},
	_getCriteria: function () {
		var from = moment(this._dateFrom.getDate());
		var to = moment(this._dateTo.getDate());
		var criteria = {
			dateFrom: from.isValid() ? from.format('YYYY-MM-DD 00:00:00.000') : '',
			dateTo: to.isValid() ? to.format('YYYY-MM-DD 23:59:59.999') : '',
			id: this._jqe.find('#id').val(),
			displayName: this._jqe.find('#displayName').val()
		};
		return criteria;
	},
	_clearResult: function () {
		this._resultTable.find('tbody').html('');
	},
	_reset: function () {
		var today = moment();
		var yesterday = moment().subtract(1, 'day');
		this._dateFrom.setDate(yesterday.format('YYYY/MM/DD'));
		this._dateTo.setDate(today.format('YYYY/MM/DD'));
		this._jqe.find('#id').val('');
		this._jqe.find('#displayName').val('');
		this._jqe.find('#goBtn').focus();
		this._clearResult();
	}
});
butor.sec.audit.ServiceCall = butor.sec.audit.ServiceCall || butor.Class.define({
	_jqe: null,
	_resultTable: null,
	_dateTo: null,
	_dateFrom: null,
	tr: butor.sec.tr,
	construct: function (jqe) {
		this._jqe = jqe;
		this._jqe.find('#goBtn').click($.proxy(this._go, this));
		this._jqe.find('#exportBtn').click($.proxy(this._export, this));
		this._jqe.find('#resetBtn').click($.proxy(this._reset, this));
		this._resultTable = this._jqe.find('#resultTable');
		this._dateFrom = new butor.DatePicker(this._jqe.find('.dateFromGroup'), { 'errorMsg': this.tr('Date from is not valid!') });
		this._dateTo = new butor.DatePicker(this._jqe.find('.dateToGroup'), { 'errorMsg': this.tr('Date to is not valid!') });
		this._jqe.keypress($.proxy(function (e) {
			if (e.keyCode === 13) {
				this._go();
				return false;
			}
		}, this));
		this._resultTable.tablesorter();
		this._reset();
	},
	_go: function () {
		var reqId, criteria = this._getCriteria();
		if (!this._validateCriteria(criteria)) {
			return;
		}
		App.mask(this.tr('Loading'));
		reqId = butor.sec.audit.AuditAjax.listServiceCall(criteria, $.proxy(function (result) {
			console.log(result);
			var i, row, l, tbody = this._resultTable.find('tbody'), res;
			this._clearResult();
			App.unmask();
			if (!result || result.hasError || !result.data || reqId !== result.reqId) {
				return;
			}
			for (i = 0, l = result.data.length; i < l; i = i + 1) {
				res = result.data[i];
				//IMPORTANT escape fields to use with .html() or set them with .text()
				//butor.Utils.escape(res, butor.sec.audit.serviceCallFte);
				
				row = $('<tr>');
				row.append($('<td>').text(i + 1));
				row.append($('<td>').text(butor.Utils.formatDateTime(res.serviceCallTimestamp)));
				row.append($('<td>').text(res.user));
				row.append($('<td>').text(res.sessionId));
				row.append($('<td>').text(res.namespace));
				row.append($('<td>').text(res.service));
				row.append($('<td>').text(res.success));
				row.append($('<td>').text(res.duration));
				row.append($('<td>').text(res.payload));
				var attributes = res.attributesMap;
				if (attributes) {
					if (butor.Utils.isEmpty(attributes.reverseLookup)) {
						row.append($('<td>').text(butor.Utils.escapeStr(attributes.ip)));
					} else {
						row.append($('<td>').text(butor.Utils.escapeStr(attributes.ip + ' / ' + attributes.reverseLookup)));
					}
					row.append($('<td>').text(butor.Utils.escapeStr(attributes.domain)));
					row.append($('<td>').text(butor.Utils.escapeStr(attributes.userAgent)));
				}
				tbody.append(row);
			}
			this._resultTable.trigger('update');
		}, this));
	},
	_export: function () {
		var reqId, criteria = this._getCriteria();
		if (!this._validateCriteria(criteria)) {
			return;
		}
		App.mask(this.tr('Loading'));
		reqId = butor.sec.audit.AuditAjax.exportServiceCall(criteria, function (result) {
			return;
		});
		App.unmask();
	},
	_validateCriteria: function (criteria) {
		var valid = false;
		if (criteria) {
			if (butor.Utils.isEmpty(criteria.dateFrom)) {
				this._jqe.find('.dateFromGroup input').focus();
				App.msgError(this.tr('Date from is mandatory'));
				return false;
			}
			if (butor.Utils.isEmpty(criteria.dateTo)) {
				this._jqe.find('.dateToGroup input').focus();
				App.msgError(this.tr('Date to is mandatory'));
				return false;
			}
			valid = true;
		}
		return valid;
	},
	_getCriteria: function () {
		var from = moment(this._dateFrom.getDate());
		var to = moment(this._dateTo.getDate());
		var criteria = {
			dateFrom: from.isValid() ? from.format('YYYY-MM-DD 00:00:00.000') : '',
			dateTo: to.isValid() ? to.format('YYYY-MM-DD 23:59:59.999') : '',
			user: this._jqe.find('#user').val(),
			session: this._jqe.find('#sessionId').val(),
			service: this._jqe.find('#serviceName').val()
		};
		return criteria;
	},
	_clearResult: function () {
		this._resultTable.find('tbody').html('');
	},
	_reset: function () {
		var today = moment();
		var yesterday = moment().subtract(1, 'day');
		this._dateFrom.setDate(yesterday.format('YYYY/MM/DD'));
		this._dateTo.setDate(today.format('YYYY/MM/DD'));
		this._jqe.find('#id').val('');
		this._jqe.find('#user').val('');
		this._jqe.find('#sessionId').val('');
		this._jqe.find('#serviceName').val('');
		this._jqe.find('#goBtn').focus();
		this._clearResult();
	}
});
//# sourceURL=bsec.audit.js
