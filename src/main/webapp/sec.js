/*
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
if (!butor.sec) {
	butor.sec = {
		ACCESS_MODE_READ : 1,
		ACCESS_MODE_WRITE : 2,
		pages : ['firms', 'users', 'groups', 'funcs', 'roles', 'auths', 'audit'],
		tr : function(txt) {
			return App.Bundle.get(txt, 'sec');
		},
		start : function() {
			for (var i=0; i<butor.sec.pages.length; i+=1) {
				var func = butor.sec.pages[i];
				var pageId = 'sec.' +func;
				if (App.hasAccess('sec', func)) {
					App.showPage(pageId);
					break
				}
			}
		}
	};

	App.Bundle.add('sec', {
		'Security': {'fr': 'Sécurité'},
		'Add user': {'fr': 'Ajouter usager'},
		'Create user': {'fr': 'Créer usager'},
		'Users': {'fr': 'Usagers'},
		'User': {'fr': 'Usager'},
		'Add group': {'fr': 'Créer groupe'},
		'Groups': {'fr': 'Groupes'},
		'Group': {'fr': 'Groupe'},
		'Firms': {'fr': 'Firmes'},
		'Roles': {'fr': 'Rôles'},
		'Role': {'fr': 'Rôle'},
		'Groups': {'fr': 'Groupes'},
		'All groups': {'fr': 'Toutes les groupes'},
		'Group users': {'en' : "Group's users", 'fr': 'Usagers du groupe'},
		'User lookup': {'fr': 'Recherche usager'},
		'Selected users': {'fr': 'Usagers séléctionnés'},
		'All users': {'fr': 'Tous les usagers'},
		'Selected firms': {'fr': 'Firmes séléctionnées'},
		'Add selected': {'fr': 'Ajouter sélection'},
		'Remove selected': {'fr': 'Supprimer sélection'},
		'All firms': {'fr': 'Toutes les firmes'},
		'Available firms': {'fr': 'Firmes disponibles'},
		'Add role': {'fr': 'Ajouter rôle'},
		'Add group': {'fr': 'Ajouter groupe'},
		'Add firm': {'fr': 'Ajouter firme'},
		'Add function': {'fr': 'Créer fonction'},
		'Reset/Send link': {'fr': 'Initialiser/Envoyer lien'},
		'Reset/Show link': {'fr': 'Initialiser/Afficher lien'},
		'Are you sure you want to reset this user login?': {'fr': 'Êtes-vous certain de vouloir initialiser le login de cet usager?'},
		'Reset done' : {'fr':'Initialisation effectuée'},
		'Copy this link' : {'fr' :'Copier ce lien'},
		'Reset link' : {'fr' : "Lien d'initialisation"},
		'Reset on which domain?' : {'fr':'Initialiser sur quel domaine?'},
		'Functions': {'fr': 'Fonctions'},
		'System & Function': {'fr': 'Système & Fonction'},
		'All functions': {'fr': 'Toutes les fonctions'},
		'Function': {'fr': 'Fonction'},
		'Role functions': {'en' : "Role's functions", 'fr': 'Fonctions du rôle'},
		'Available functions': {'fr': 'Fonctions disponibles'},
		'Selected functions': {'fr': 'Fonctions séléctionnées'},
		'First name': {'fr': 'Prénom'},
		'Last name': {'fr': 'Nom'},
		'Display name': {'fr': 'Nom affichage'},
		'Full name': {'fr': 'Nom complet'},
		'Contact name': {'fr': 'Nom contact'},
		'Contact phone': {'fr': 'No tél. contact'},
		'Reset in progress': {'fr': 'Initialisation en cours'},
		'Locked': {'fr': 'Verrouillé'},
		'New password': {'fr': 'Nouveau mot de passe'},
		'Confirm new password': {'fr': 'Confirmation mot de passe'},
		'New passwords do not match': {'fr': 'Nouveaux mots de passes ne sont pas identiques'},
		'Fixed Income Category': {'fr': 'Catégorie revenu fixe'},
		"Authorisation": {'fr': 'Autorisation'},
		"Authorisations": {'fr': 'Autorisations'},
		"Data": {'fr': 'Données'},
		"Authorise": {'fr': 'Autorise'},
		"On": {'fr': 'Pour'},
		"System": {'fr': 'Système'},
		"On data": {'fr': 'Avec données'},
		"Start date": {'fr': 'Date début'},
		"End date": {'fr': 'Date fin'},
		"Access mode": {'fr': "Mode d'accès"},
		"Add data bundle": {'fr': 'Ajouter données'},
		"Phone": {'fr': 'Téléphone'},
		"Remove user": {'fr': "Supprimer l'usager"},
		"Remove firm": {'fr': "Supprimer la firme"},
		'Phone': {'fr': 'Téléphone'},
		'On': {'fr': 'À'},
		'Allowed domains': {'fr': 'Domaines authorisés'},
		'coma separated domains': {'fr': 'Domaines séparés par virgule'},
		'Two steps sign in': {'fr': 'Identification en deux étapes'},
		'Last login':{'fr' : 'Dernière connection'},
		'ID':{'fr' : 'Identifiant'},
	    'On domain':{'fr' : 'Sur domaine'},
	    'IP / Reverse lookup':{'fr' : 'IP / Recherche inversée'},
		'Date from':{'fr' : 'Du'},
		'Date to':{'fr' : 'Au'},
		'Date from is mandatory':{'fr' : 'Le champ "Du" est obligatoire'},
        'Date to is mandatory':{'fr' : 'Le champ "Au" est obligatoire'}
	});
	App.Bundle.add('common', {
		"CANNOT_DELETE_REFERED_ROLE": {'en': 'Cannot delete role that is refered by authorisation!',
			'fr': 'Il n\'est pas possible de supprimer un rôle référé dans autorisation!'},
	});
}

butor.sec.auth = butor.sec.auth || function() {
	var tr = butor.sec.tr;
	var _authDataEditorDef = {};
	return {
		fte : ["authId", "who", "what"],
		registerFactory: function(type, name, factory) {
			if (!_authDataEditorDef[type]) {
				_authDataEditorDef[type] = {'factory' : factory, 'name':name};
			}
		},
		createDataEditor : function(args) {
			var cmpDef = _authDataEditorDef[args.dataType];
			if (cmpDef) {
				return cmpDef.factory({'elem':args.elem, 'msgPanel':args.msgPanel});
			} else {
				args.msgPanel.error(tr('No editor found for data type:') +' ' +args.dataType);
			}
			return null;
		},
		getDataEditorNames : function() {
			var list = {};
			for (dt in _authDataEditorDef) {
				list[dt] = _authDataEditorDef[dt].name || dt;
			}
			return list;
		}/*,
		listUserAuthFunc: function(args, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:true, service:'listUserAuthFunc'},
				[args], handler);
		}*/
	}
}();


butor.sec.auth.AuthDataBaseEditor = butor.sec.auth.AuthDataBaseEditor || butor.Class.define({
	_jqe : null,
	_showAllOption : true,
	_selection : null,
	_reqId : null,
	_isEdit : false,
	_selectionMap : {},
	_msgPanel : null,
	_availableItems : null,
	_selectionSB : null,
	_itemsSB : null,
	_isReady : false,
	_enabled : false,
	_cReqId : null,
	_allCB : null,
	_allCBLbl : null,
	tr : function(txt) {
		//TODO override
		return txt;
	},
	/**
	 * args.htmlFile : html file of the editor
	 * args.elem : dom jquery element where to render the editor
	 * args.showAllOption : show the "All" checkbox option
	 * args.prefix : prefix of elements name and css class
	 * args.sys : system id to use for translation
	 */
	construct : function(args) {
		this._jqe = args.elem;
		this._msgPanel = args.msgPanel;
		this._showAllOption = (args.showAllOption === undefined || args.showAllOption);
		this._cssEditableClass = '.' +args.prefix +'editable';
		this._jqe.load(args.htmlFile, null, $.proxy(function() {
			App.translateElem(this._jqe, args.sys);
			this._allCBLbl = this._jqe.find('#' +args.prefix +'allCB');
			this._allCB = this._allCBLbl.find('input');
			this._itemsSB = this._jqe.find('#' +args.prefix +'itemsSB');
			this._selectionSB = this._jqe.find('#' +args.prefix +'selectionSB');

			if (this._showAllOption) {
				this._allCBLbl.show();
				this._allCB.change($.proxy(this._allSelectionChanged, this));
			} else {
				this._allCBLbl.hide();
			}
		
			this._jqe.find('#' +args.prefix +'removeBtn').click($.proxy(function() {
				this._removeSelectedItems();
			}, this)).attr('title', this.tr('Remove'));
			this._jqe.find('#' +args.prefix +'addBtn').click($.proxy(function() {
				this._addSelectedItems();
			}, this)).attr('title', this.tr('Add'));
			this._showAvailItems();
			this._ready();
		}, this));
	},
	_ready : function() {
		this._isReady = true;
		this.fire('ready', null);
	},
	isReady : function() {
		return this._isReady;
	},
	_allSelectionChanged : function() {
		if (this._allCB.is(":checked")) {
			this._jqe.find(this._cssEditableClass).butor('disable');
			if (this._enabled) {
				this._allCB.butor('enable');
			}
			this._clearItems();
			this._selectionSB.empty().butor('disable');
			this._selectionMap = {};
		} else {
			this._jqe.find(this._cssEditableClass).butor('enable');
			this._selectionSB.butor('enable');
			this._showAvailItems();
		}
	},
	_clearItems : function() {
		this._itemsSB.empty();
	},
	_fillItemsSB : function() {
		var sb = this._itemsSB;
		sb.empty();
		for (var id in this._availableItems) {
			if (this._selectionMap[id]) {
				// already in selection
				continue;
			}
			sb.append('<div class="checkbox"><label> <input value="' +id +'" type="checkbox">' +
				butor.Utils.escapeStr(this._formatItem(id)) +'</label></div>');
		}
	},
	_showAvailItems : function() {
		if (this._availableItems) {
			this._fillItemsSB();
			return;
		}
		this._availableItems = {};
		this._msgPanel.hideMsg();
		this._loadItems($.proxy(function(data) {
				if (!data) {
					return;
				}
				for (var ii=0; ii<data.length; ii++) {
					var item = data[ii];
					this._availableItems[item.id] = item;
				}
				this._fillItemsSB();
				if (this._selection) {
					if (this._enabled) {
						this.enable();
					} else {
						this.disable();
					}
					this._setSelectedKeys(this._selection);
				}
			}, this)
		);
	},
	enable : function() {
		this._enabled = true;
		if (!this._isReady) {
			return;
		}
		this._jqe.find(this._cssEditableClass).butor('enable');
		this._selectionSB.butor('enable');
		this._allSelectionChanged(); //set enability depending on checkbox
	},
	disable : function() {
		this._enabled = false;
		this._jqe.find(this._cssEditableClass).butor('disable');
		this._itemsSB.empty();
	},
	_addSelectedItems : function() {
		var self = this;
		this._itemsSB.find('input:checked').each(function(index, elem) {
			var id = $(this).attr('value');
			if (self._selectionMap[id]) {
				// selected already
				return;
			}
			var opt = $('<div class="checkbox"><label> <input value="' +id +'" type="checkbox">' +
				butor.Utils.escapeStr(self._formatSelectedItem(id)) +'</label></div>');
			self._selectionSB.append(opt);
			$(this).parent().parent().remove();
			self._selectionMap[id] = {'id':id};
		});
	},
	_removeSelectedItems : function() {
		var self = this;
		this._selectionSB.find('input:checked').each(function(index, elem) {
			var item = $(this);
			var id = item.attr('value');
			self._recycleItem(id);
			$(this).parent().parent().remove();
			delete self._selectionMap[id];
		});
	},
	_recycleItem : function(id) {
		if (id) {
			var opt = $('<div class="checkbox"><label> <input value="' +id +'" type="checkbox">' +
				butor.Utils.escapeStr(this._formatItem(id)) +'</label></div>');
			this._itemsSB.append(opt);
		}
	},
	_loadItems : function(callback) {
		//TODO override
		alert('override _loadItems(callback)!');
	},
	_formatItem : function(key) {
		// override
		var item = this._getItem(key);
		return item.id;
	},
	_getItem : function(key) {
		return this._availableItems[key];
	},
	_formatSelectedItem : function(key) {
		// override if required
		return this._formatItem(key);
	},
	_getSelectedKeys : function() {
		var data = [];
		if (this._showAllOption && this._allCB.attr('checked')) {
			data.push('*');
			return data;
		}
		for (key in this._selectionMap) {
			data.push(key);
		}
		return data;
	},
	_setSelectedKeys : function(selection) {
		this._selection = selection;
		if (!this._isReady) {
			return;
		}
		this._itemsSB.empty();
		this._selectionMap = {};
		if (!this._selection) {
			this._showAvailItems()
			return;
		}

		var sb = this._selectionSB;
		sb.empty();
		for (var ii=0,c=this._selection.length; ii<c; ii+=1) {
			var id = this._selection[ii];
			if (id == '*') {
				this._allCB.attr('checked', true).change();
				return;
			}
			//var opt = $("<option value=\"" +id +"\">" +this._formatSelectedItem(id) +"</option>");
			var opt = $('<div class="checkbox"><label> <input value="' +id +'" type="checkbox">' +this._formatSelectedItem(id) +'</label></div>');
			sb.append(opt);
			this._selectionMap[id] = id;
		}
		this._showAvailItems();
	},
	getData : function() {
		//TODO override
		alert('override getData()!');
		/*
		var items = this._getSelectedKeys();
		var data = [];
		for (var i=0,c=items.length; i<c; i+=1) {
			data.push({'sys':'sec','dataType':'role','d1':items[i]});
		}
		return data;
		*/
		return [];
	},
	setData : function(data) {
		//TODO override
		alert('override setData()!');
		/*
		var items = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				items.push(data[i].d1);
			}
		}
		this._setRoles(items);
		*/
	}
});

butor.sec.firm = butor.sec.firm || {
	fte : ["firmId", "firmName", "contactName", "contactPhone"]
};

butor.sec.firm.Firms = butor.sec.firm.Firms || butor.Class.define({
	_selectedItem : null,
	_selectedItemFields : ['firmId', 'revNo', 'firmName'],
	_reqId : null,
	_ctxMenu : null,
	_jqe : null,
	_canEdit : null,
	_tableCtxtMenu : null,
	tr : butor.sec.tr,
	construct : function(jqe) {
		this._jqe = jqe;
		var listFirmsP = $.proxy(this._listFirms, this);
		this._canEdit = App.hasAccess('sec', 'firms', butor.sec.ACCESS_MODE_WRITE);
		this._jqe.find('#resultTable').tablesorter({textExtraction : 
			function(node) {
				var val = node.textContent;
				if (val == '') {
					val = node.innerHTML;
				}
				return val;
			}
		});
		
		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li a.view').click($.proxy(function(e_) {
			var dlg = butor.sec.firm.FirmDlg.view(this._selectedItem.firmId);
			if (dlg) {
				dlg.bind('firm-updated', listFirmsP);
			}
		}, this));
		if (this._canEdit) {
			this._ctxMenu.find('li.edit').show().find('a').click($.proxy(function(e_) {
				var dlg = butor.sec.firm.FirmDlg.edit(this._selectedItem.firmId);
				if (dlg) {
					dlg.bind('firm-updated', listFirmsP);
				}
			}, this));
			this._ctxMenu.find('li.remove').show().find('a').click($.proxy(function(e_) {
				if (!confirm(butor.sec.tr('Remove firm') +' ' +this._selectedItem.firmName +'?')) {
					return null;
				}
				var rReqId = butor.sec.firm.FirmAjax.deleteFirm(this._selectedItem, $.proxy(function(result){
					if (!AJAX.isSuccess(result, rReqId)) {
						return;
					}
					// refresh
					this._listFirms();
				}, this));
			}, this));
			this._jqe.find('#addBtn').show().click($.proxy(function(e_){
				this.newFirm();
			}, this));
		}
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._jqe.find('#resultTable'), this._ctxMenu);
		var self = this;
		this._jqe.find('#resultTable tbody').delegate('tr', 'hover', function() {
			var row = $(this);
			self._selectedItem = {};
			for (i in self._selectedItemFields) {
				var f = self._selectedItemFields[i];
				self._selectedItem[f] = row.attr(f);
			}
		});
		this._jqe.find('#goBtn').click(listFirmsP);
		this._jqe.find('#resetBtn').click($.proxy(function(e_){
			this._jqe.find('#firm').val('');
			this.clear();
		}, this));

		this._jqe.find(".form-inline").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listFirms();
				return false;
			}
		}, this));
		this._jqe.find("#resultTable").delegate('a.firm-link', 'click', $.proxy(function(e_) {
			var dlg = butor.sec.firm.FirmDlg.view(this._selectedItem.firmId);
			if (dlg) {
				dlg.bind('firm-updated', $.proxy(this._listFirms, this));
			}
		}, this));
	},
	newFirm : function() {
		var dlg = butor.sec.firm.FirmDlg.newFirm();
		if (dlg) {
			dlg.bind('firm-updated', $.proxy(this._listFirms, this));
		}
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		var tbody = this._jqe.find('#resultTable > tbody');
		tbody.empty();
	},
	_listFirms : function() {
		App.hideMsg();
		this.clear();
		var re = null;
		var firm = this._jqe.find('#firm').val();
		if (firm) {
			re = new RegExp(firm, "gi");
		}

		App.mask(this.tr('Loading'));
		this._cReqId = butor.sec.firm.FirmAjax.listFirm({sys:"sec", func:"firms", mode:"READ"}, 
			$.proxy(function(result) {
				App.unmask();
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}

				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<result.data.length; ii++) {
					var firm = result.data[ii];
					if (re && !(firm.contactName +' ' +firm.firmName +' ' +firm.contactPhone).match(re)) {
						continue;
					}
					tbody.append(this._createRow(firm));
				}
				this._jqe.find("#resultTable").trigger("update");
			}, this)
		);
	},
	_createRow : function(firm) {
		butor.Utils.escape(firm, butor.sec.firm.fte);
		var row = $("<tr class=\"firm\">");
		row.html("<td class=\"number\">" +firm.firmId +"</td><td><a class=\"firm-link\">" +firm.firmName +"</a></td>" +
				"<td>" +firm.contactName +"</td><td>" +firm.contactPhone +'</td><td class="align-center">' +(firm.active ? "<i class=\"fa fa-check\"></i>" : '') +"</td>" +
				"<td>" +butor.Utils.formatDateTime(firm.creationDate) +"</td><td>" +butor.Utils.formatDateTime(firm.stamp) +"</td><td>" +firm.userId +"</td>");

		row.attr('firmName', firm.firmName).attr('revNo', firm.revNo).attr('firmId', firm.firmId); 
		return row;
	}
});

butor.sec.firm.FirmDlg = butor.sec.firm.FirmDlg || butor.dlg.Dialog.define({
	statics : {
		newFirm : function(firm) {
			var args = {'firm':firm, 'mode':'add'};
			return butor.sec.firm.FirmDlg.show(args);
		},
		view : function(firmId) {
			var args = {'firmId':firmId, 'mode':'view'};
			return butor.sec.firm.FirmDlg.show(args);
		},
		edit : function(firmId) {
			var args = {'firmId':firmId, 'mode':'edit'};
			return butor.sec.firm.FirmDlg.show(args);
		},
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("Firm"),
				width : 500,
				height : 430,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.firm.FirmDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_firm : null,
	_reqId : null,
	_isEdit : false,
	_canUpdate : App.hasAccess('sec', 'firms', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/firmDlg.html');
	},
	_init : function() {
		this.base();
		App.translateElem(this._jqe, 'sec');
		this._jqe.find('#allowedDomains').attr('placeholder', this.tr('coma separated domains'));

		this._jqe.find('#cancelBtn').click($.proxy(function() {
			if (this._isEdit)
				this.close();
			else
				this._disableForm();
		}, this));
		this._jqe.find('#closeBtn').click($.proxy(this.close, this));
		this._jqe.find('#saveBtn').click($.proxy(this._save, this));
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').click($.proxy(this._editMode, this));
		}

		switch (this._args.mode) {
		case 'add':
			this._newFirm(this._args.firm);
			break;
		case 'view':
			this._view(this._args.firmId);
			break;
		case 'edit':
			this._edit(this._args.firmId);
			break;
		}
	},
	_editMode : function() {
		this._jqe.find('.editable').removeAttr('disabled');
		this._jqe.find('#closeBtn').hide();
		this._jqe.find('#modifyLink').hide();
		this._jqe.find('#cancelBtn').show();
		this._jqe.find('#saveBtn').show();
		this.hideMsg();
	},
	_disableForm : function() {
		this._jqe.find('.editable').butor('disable');
		this._jqe.find("button.btn").hide();
		this._jqe.find('#closeBtn').show();
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').show();
		}
		this._showFirmInfo();
	},
	_save : function() {
		var args = this._getData();
		
		this.mask(this.tr("Saving"));
		this.hideMsg();
		this._reqId = butor.sec.firm.FirmAjax.updateFirm(args, {'msgHandler':this.getMsgPanel(), 
			'scope':this, 'callBack':function(result) {
			this.unmask();
			if (!AJAX.isSuccessWithData(result, this._reqId)) {
				return;
			}
			this.showMsg(this.tr('Saved'));
			this._firm.firmId = result.data[0].firmId;
			this.fire('firm-updated', this._firm.firmId);
			this._view(result.data[0].firmId);
		}});
	},
	_getData : function() {
		var data = {};
		data.firmId = this._firm.firmId;
		data.revNo = this._firm.revNo;
		data.firmName = this._jqe.find("#firmName").val();
		data.contactName = this._jqe.find("#contactName").val();
		data.contactPhone = this._jqe.find("#contactPhone").val();
		data.active = this._jqe.find("#active:checked").length == 1;
		data.attributesMap = data.attributesMap || {};
		data.attributesMap.allowedDomains = this._jqe.find("#allowedDomains").val();
		data.attributesMap.twoStepsSignin = this._jqe.find("#twoStepsSignin:checked").length == 1;

		return data;
	},
	_newFirm : function(firm) {
		this._isEdit = true;
		this._firm = firm;
		if (this._firm === undefined) {
			this._firm = {revNo:null, firmId:null};
		} else {
			this._showFirmInfo();
		}
		this._editMode();
	},
	_view : function(firmId, isEdit) {
		this._isEdit = isEdit;
		this._disableForm();
		this.hideMsg();
		this.mask(this.tr("Initializing"));
		this._reqId = butor.sec.firm.FirmAjax.readFirm(firmId, "sec", "firms", "READ", {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				this._firm = result.data[0];
				
				this._showFirmInfo();
				if (this._isEdit)
					this._editMode();
		}});
	},
	_edit : function(firmId) {
		this._view(firmId, true);
	},
	_showFirmInfo : function() {
		if (!this._firm) {
			return;
		}
		this._jqe.find("#firmName").val(this._firm.firmName);
		this._jqe.find("#contactName").val(this._firm.contactName);
		this._jqe.find("#contactPhone").val(this._firm.contactPhone);
		this._jqe.find("#active").attr("checked", this._firm.active);
		var twoStepsSignin = true; // yes by default
		if (this._firm.attributesMap) {
			if (this._firm.attributesMap.allowedDomains) {
				this._jqe.find("#allowedDomains").val(this._firm.attributesMap.allowedDomains);
			}
			twoStepsSignin = (this._firm.attributesMap.twoStepsSignin == null || this._firm.attributesMap.twoStepsSignin);
		}
		this._jqe.find("#twoStepsSignin").attr("checked", twoStepsSignin);
	}
});

butor.sec.firm.AuthDataFirmEditor = butor.sec.firm.AuthDataFirmEditor || butor.sec.auth.AuthDataBaseEditor.extend({
	_reqId : null,
	tr : butor.sec.tr,

	construct : function(args) {
		this._jqe = args.elem;
		args.prefix = 'adfe-';
		args.sys = 'sec';
		args.htmlFile = '/bsec/authDataFirmEditor.html';
		this.base(args);
	},
	_loadItems : function(callback) {
		this._reqId = butor.sec.firm.FirmAjax.listFirm({streaming : false, sys:"sec", func:"firms", mode:"READ"}, {msgHandler:this._msgPanel, 
			'scope':this, callBack:function(result) {
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					callback(null);
					return;
				}
				for (var i=0;i<result.data.length;i+=1) {
					result.data[i].id = result.data[i].firmId;
				}
				callback(result.data);
			}
		});
	},
	_formatItem : function(key) {
		var firm = this._getItem(key);
		return (firm ? firm.firmName : key);
	},
	getData : function() {
		var firms = this._getSelectedKeys();
		var data = [];
		for (var i=0,c=firms.length; i<c; i+=1) {
			data.push({'sys':'sec','dataType':'firm','d1':firms[i]});
		}
		return data;
	},
	setData : function(data) {
		var firms = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				firms.push(data[i].d1);
			}
		}
		this._setSelectedKeys(firms);
	}
});
if (!butor.sec.auth.getDataEditorNames()['firm']) {
	butor.sec.auth.registerFactory('firm', 'Firm', function(args) {
		return new butor.sec.firm.AuthDataFirmEditor(args);
	});
}

butor.sec.firm.FirmAjax = function() {
	return {
		listFirm: function(args, handler) {
			return AJAX.call('/bsec/firm.ajax', 
				{streaming:false, service:'listFirm'},
				[args], handler);
		},
		readFirm: function(id, sys, func, mode, handler) {
			return AJAX.call('/bsec/firm.ajax', 
				{streaming:false, service:'readFirm'},
				[id, sys, func, mode], handler);
		},
		deleteFirm: function(firm, handler) {
			return AJAX.call('/bsec/firm.ajax', 
				{streaming:false, service:'deleteFirm'},
				[firm], handler);
		},
		updateFirm: function(args, handler) {
			return AJAX.call('/bsec/firm.ajax', 
				{streaming:false, service:(args.firmId?'updateFirm':'insertFirm')},
				[args], handler);
		}
	}
}();

//===================
butor.sec.user = butor.sec.user || {
	fte : ["id", "firmName", "firstName", "lastName", "displayName", "email"]
};

butor.sec.user.Users = butor.sec.user.Users || butor.Class.define({
	_selectedItem : null,
	_selectedItemFields : ['id', 'revNo', 'firstName', 'lastName', 'displayName', 'email'],
	_reqId : null,
	_reqId2 : null,
	_cReqId : null,
	_ctxMenu : null,
	_firms : {},
	_jqe : null,
	_tableCtxtMenu : null,
	_canUpdate : App.hasAccess('sec', 'users', butor.sec.ACCESS_MODE_WRITE),
	construct : function(jqe) {
		this._jqe = jqe;
		this._reqId2 = butor.sec.firm.FirmAjax.listFirm({streaming : false, sys:"sec", func:"firms", mode:"READ"}, $.proxy(function(result) {
			if (!AJAX.isSuccessWithData(result, this._reqId2)) {
				return;
			}
			var firms = result.data;
			var sb = this._jqe.find('#firmId');
			sb.empty();
			var opt = $("<option>");
			opt.attr("value", "").html("");
			opt.appendTo(sb);
			for ( var cc = 0; cc < firms.length; cc++) {
				var firm = firms[cc];
				butor.Utils.escape(firm, butor.sec.firm.fte);
				opt = $("<option>");
				opt.attr("value", firm.firmId).html(firm.firmName);
				opt.appendTo(sb);
				this._firms[firm.firmId] = firm.firmName;
			}
		}, this));

		this._jqe.find('#resultTable').tablesorter({textExtraction : 
			function(node) {
				var val = node.textContent;
				if (val == '') {
					val = node.innerHTML;
				}
				return val;
			}
		});

		var listUsersP = $.proxy(this._listUsers, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			var dlg = butor.sec.user.UserDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('user-updated', listUsersP);
			}
		}, this));
		if (this._canUpdate) {
			this._ctxMenu.find('li.add').show().find('a').click($.proxy(function(e_) {
				var row = this._tableCtxtMenu.getHoveredRow();
				var user = row.data('data');
				var dlg;
				if (butor.Utils.isEmpty(user.creationDate)) {
					user.revNo = null;
					dlg = butor.sec.user.UserDlg.newUser(user);
				} else {
					dlg = butor.sec.user.UserDlg.edit(user.id);
				}
				if (dlg) {
					dlg.bind('user-updated', listUsersP);
				}
			}, this));
			this._ctxMenu.find('li.edit').show().find('a').click($.proxy(function(e_) {
				var user = this._tableCtxtMenu.getHoveredItem();
				var dlg = butor.sec.user.UserDlg.edit(user.id);
				if (dlg) {
					dlg.bind('user-updated', listUsersP);
				}
			}, this));
			this._ctxMenu.find('li.delete').show().find('a').click($.proxy(function(e_) {
				var rReqId = butor.sec.user.UserAjax.deleteUser(this._tableCtxtMenu.getHoveredItem(), $.proxy(function(result){
					if (!AJAX.isSuccessWithData(result, this._rReqId)) {
						return;
					}
					// refresh
					this._listUsers();
				}, this));
			}, this));
			this._jqe.find('#addBtn').show().click($.proxy(function(){
				var dlg = butor.sec.user.UserDlg.newUser();
				if (dlg) {
					dlg.bind('user-updated', listUsersP);
				}
			}, this));
		}
		if (App.hasAccess('sec', 'groups', butor.sec.ACCESS_MODE_READ)) {
			this._ctxMenu.find('li.groups').show().find('a').click($.proxy(function(e_) {
				butor.sec.group.Groups.userGroups(this._tableCtxtMenu.getHoveredItem().id);
			}, this));
		}
		if (App.hasAccess('sec', 'auths', butor.sec.ACCESS_MODE_READ)) {
			this._ctxMenu.find('li.auths').show().find('a').click($.proxy(function(e_) {
				var member = this._tableCtxtMenu.getHoveredItem().id;
				App.showPage('sec.auths', {member:member});
			}, this));
		}
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._jqe.find('#resultTable'), this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			if (butor.Utils.isEmpty(e.data.revNo)) {
				this._ctxMenu.find('li.delete').hide();
				this._ctxMenu.find('li.groups').hide();
				this._ctxMenu.find('li.auths').hide();
				this._ctxMenu.find('li.view').hide();
				this._ctxMenu.find('li.edit').hide();
				if (this._canUpdate) {
					this._ctxMenu.find('li.add').show();
				}
			} else {
				this._ctxMenu.find('li.groups').show();
				this._ctxMenu.find('li.auths').show();
				this._ctxMenu.find('li.view').show();
				this._ctxMenu.find('li.add').hide();
				if (this._canUpdate) {
					this._ctxMenu.find('li.edit').show();
					this._ctxMenu.find('li.delete').show();
				}
			}
		}, this));

		this._jqe.find('#goBtn').click(listUsersP);
		this._jqe.find('#resetBtn').click($.proxy(function(e_){
			this._jqe.find('#firmId').val('');
			this._jqe.find('#displayName').val('');
			this.clear();
		}, this));
		this._jqe.find('#firmId').change(listUsersP);

		this._jqe.find(".form-inline").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listUsers();
				return false;
			}
		}, this));

		this._jqe.find("#resultTable").delegate('a.user-link', 'click', $.proxy(function(e_) {
			var dlg = butor.sec.user.UserDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('user-updated', $.proxy(this._listUsers, this));
			}
		}, this));

	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		var tbody = this._jqe.find('#resultTable > tbody');
		tbody.empty();
	},
	_listUsers : function() {
		App.hideMsg();
		this.clear();
		var crit = {};
		var val = this._jqe.find('#firmId').val();
		if (val != '')
			crit.firmId = val;

		val = this._jqe.find('#displayName').val();
		if (val != '')
			crit.displayName = val;

		this._cReqId = butor.sec.user.UserAjax.listUser(crit, null,
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}
				var data = result.data;
				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<data.length; ii++) {
					var user = data[ii];
					butor.Utils.escape(user, butor.sec.user.fte);
					var row = $("<tr class=\"user\">");
					row.html("<td class=\"number\">" +(ii+1) +"</td><td>" +
							(butor.Utils.isEmpty(user.creationDate) ? user.id : "<a class=\"user-link\">" +user.id +"</a>")+
							"</td><td>" +user.displayName +"</td><td>" +
							(user.firmName ? user.firmName : this._firms[user.firmId]) +
							'</td><td class="align-center">' +(user.active ? "<i class=\"fa fa-check\"></i>" : '') +
							"</td><td>" +
							(user.creationDate ? butor.Utils.formatDateTime(user.creationDate) : '') +
							"</td><td>" +
							(user.stamp ? butor.Utils.formatDateTime(user.stamp) : '') +"</td>");

					row.attr('id', user.id);
					row.attr('firstName', user.firstName);
					row.attr('lastName', user.lastName);
					row.attr('displayName', user.displayName);
					row.attr('email', user.email);
					if (!butor.Utils.isEmpty(user.creationDate)) {
						row.attr('revNo', user.revNo);
					}
					row.data('data',user);

					tbody.append(row);
				}
				this._jqe.find("#resultTable").trigger("update");
			}, this));
	}
});

butor.sec.user.UserDlg = butor.sec.user.UserDlg || butor.dlg.Dialog.define({
	statics : {
		newUser : function(user) {
			var args = {'user':user, 'mode':'add'};
			return butor.sec.user.UserDlg.show(args);
		},
		view : function(id) {
			var args = {'id':id, 'mode':'view'};
			return butor.sec.user.UserDlg.show(args);
		},
		edit : function(id) {
			var args = {'id':id, 'mode':'edit'};
			return butor.sec.user.UserDlg.show(args);
		},
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("User"),
				width : 800,
				height : 560,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.user.UserDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_user : null,
	_reqId : null,
	_firms : {},
	_isEdit : false,
	_allGroups: [],
	_myGroups: [],
	_canUpdate : App.hasAccess('sec', 'users', butor.sec.ACCESS_MODE_WRITE),
	_canReadGroup: App.hasAccess('sec', 'groups', butor.sec.ACCESS_MODE_READ),
	_canUpdateGroup: App.hasAccess('sec', 'groups', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/userDlg.html');
	},
	_init : function() {		
		this.base();
		App.translateElem(this._jqe, 'sec');

		var reqId = butor.sec.firm.FirmAjax.listFirm({streaming:false, sys:"sec", func:"firms", mode:"READ"},
				{'msgHandler':this.getMsgPanel(),'scope':this, 'callBack':function(result) {
				if (!AJAX.isSuccessWithData(result, reqId)) {
					return;
				}
				var firms = result.data;
				var sb = this._jqe.find('#firmId');
				sb.empty();
				var opt = $("<option>");
				opt.attr("value", "").html("");
				opt.appendTo(sb);
				for (var cc=0; cc<firms.length; cc++) {
					var firm = firms[cc];
					butor.Utils.escape(firm, butor.sec.firm.fte);
					this._firms[firm.firmId] = firm;
					var opt = $("<option>");
					opt.attr("value", firm.firmId).html(firm.firmName);
					opt.appendTo(sb);
				}
				if (this._user && this._user.firmId) {
					sb.val(this._user.firmId).change();
				}
			}});

		var self = this;
		this._jqe.find('#userTabs li').click(function(e_) {
			var tabId = $(this).attr("id");
			if (tabId === 'tabUser') {
				self._jqe.find('#groupPanel').hide();
				self._jqe.find('#userPanel').addClass('active').show();
			} else if (tabId === 'tabGroup') {
				self._jqe.find('#userPanel').hide();
				self._jqe.find('#groupPanel').addClass('active').show();
			} else {
				return;
			}
			App.Utils.selectNavItem("#userTabs", "#" +tabId);
		});

		this._jqe.find('#cancelBtn').click($.proxy(function() {
			if (this._isEdit)
				this.close();
			else
				this._disableForm();
		}, this));
		this._jqe.find('#closeBtn').click($.proxy(this.close, this));
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').click($.proxy(this._editMode, this));
			this._jqe.find('#saveBtn').click($.proxy(this._save, this));
			this._jqe.find('#resetShowLink').click($.proxy(this._resetShow, this));
			this._jqe.find('#resetSendLink').click($.proxy(this._resetSend, this));
			this._jqe.find('#cancelResetBtn').click($.proxy(this._unmaskReset, this));
			this._jqe.find('#okResetBtn').click($.proxy(this._unmaskReset, this));
			this._jqe.find('#confirmResetBtn').click($.proxy(this._resetLogin, this));
		}
		this._domainTmpl = this._jqe.find('#resetOnDomain #domains .radio:first').remove();

		switch (this._args.mode) {
		case 'add':
			this._jqe.find('#tabGroup').hide();
			this._newUser(this._args.user);
			break;
		case 'view':
			this._view(this._args.id);
			break;
		case 'edit':
			this._edit(this._args.id);
			break;
		}
	},
	_editMode : function() {
		this._jqe.find('.editable').butor('enable');
		this._jqe.find('#closeBtn').hide();
		this._jqe.find('#resetShowLink').hide();
		this._jqe.find('#resetSendLink').hide();
		this._jqe.find('#modifyLink').hide();
		this._jqe.find('#cancelBtn').show();
		this._jqe.find('#saveBtn').show();
		this.hideMsg();
	},
	_disableForm : function() {
		this._jqe.find('.editable').butor('disable');
		this._jqe.find('#saveBtn').hide();
		this._jqe.find('#cancelBtn').hide();
		this._jqe.find('#closeBtn').show();
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').show();
			this._jqe.find('#resetShowLink').show();
			this._jqe.find('#resetSendLink').show();
		}
		this._jqe.find("td.pwd").addClass('hidden');
		this._showUserInfo();
	},
	_save : function() {
		var args = this._getData();
		this.mask(this.tr("Saving"));
		this.hideMsg();
		this._reqId = butor.sec.user.UserAjax.updateUser(args, {'msgHandler':this.getMsgPanel(), 
			'scope':this, 'callBack':function(result) {
			this.unmask();
			if (!AJAX.isSuccessWithData(result, this._reqId)) {
				return;
			}
			this._user.revNo = result.data[0].revNo;
			// save groups
			this._getModifiedGroups($.proxy(this._onModifiedGroupsReturned, this));
		}});
	},
	_onModifiedGroupsReturned: function(groups){
		this._saveGroups(groups, $.proxy(this._onSaveSuccess, this));
	},
	_onSaveSuccess: function(){
		this.showMsg(this.tr('Saved'));
		this._disableForm();
		this._jqe.find("#id").removeClass('editable');
		this.fire('user-updated', this._user.id);
		// clear
		this._allGroups = [];
		this._myGroups = [];
		this._loadGroupList($.proxy(this._renderGroupPanel, this));		
	},
	_saveGroups: function (groups, callback) {
		if (!this._canUpdateGroup){
			return;
		}
		var list = groups;
		var reqId;
		var handleAjaxResponse = function(result) {
			if (!AJAX.isSuccessWithData(result, reqId)) {
				return;
			}
			saveNext(list);
		}
		var saveNext = function (list){
			if (!list || list.length == 0){
				callback();
			}else{
				var g = list.shift(); // remove & return 1st
				if (!g){
					callback();
				}else{
					reqId = butor.sec.group.GroupAjax.updateGroup(g, handleAjaxResponse);
				}
			}
		};
		if (list && list.length > 0){
			var g = list.shift();
			reqId = butor.sec.group.GroupAjax.updateGroup(g, handleAjaxResponse);
		}else{
			callback();
		}
	},
	_getData : function() {
		var data = this._user;
		data.id = this._jqe.find("#id").val();
		data.email = this._jqe.find("#email").val();
		data.phone = this._jqe.find("#phone").val();
		data.firstName = this._jqe.find("#firstName").val();
		data.lastName = this._jqe.find("#lastName").val();
		data.displayName = this._jqe.find("#displayName").val();
		data.fullName = this._jqe.find("#fullName").val();
		data.firmId = this._jqe.find("#firmId").val();
		if (butor.Utils.isEmpty(data.firmId)) {
			data.firmId = -1;
		}
		data.active = this._jqe.find("#active:checked").length == 1;
		data.missedLogin = (this._jqe.find("#locked:checked").length == 1) ? 3 : 0;
		data.resetInProgress = (this._jqe.find("#resetInProgress:checked").length == 1);
		data.newPwd = this._jqe.find("#pwd1").val();
		data.newPwdConf = this._jqe.find("#pwd2").val();
		return data;
	},
	_minus: function (A, B){
		return A.filter(function (a){
			return B.indexOf(a) == -1;
		});
	},
	_getModifiedGroups: function (callback) {
		var panel = this._jqe.find('#groupPanel');
		var tbody = panel.find('#resultTable > tbody');
		var trs = tbody.find('tr');
		var uid = this._user.id;
		
		var myGroups = [];
		$.each(this._myGroups, function(i, g){
			myGroups.push(g.id);
		});
		
		var myGroupsEdited = [];
		trs.each(function(i, tr){
			var tds = $(tr).find('td');
			var checkbox = tds.find('input[type=checkbox]');
			var gid = $(tr).data('group-id')
			var isMember = checkbox.is(':checked');
			if (isMember){
				myGroupsEdited.push(gid);
			}
		});
		
		var added = this._minus(myGroupsEdited, myGroups);
		var removed = this._minus(myGroups, myGroupsEdited);

		var tobeSaved = [];
		
		var processAddded = $.proxy(function(g){
			var dfr =  $.Deferred();
			var reqId;			
			reqId = butor.sec.group.GroupAjax.readGroup(g, $.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, reqId)) {
					dfr.reject();
					return;
				}
				var groupDetail = result.data[0];
				var item = {
					member: uid,
				};
				groupDetail.items.push(item);
				tobeSaved.push(groupDetail);
				dfr.resolve();
			}, this));
			return dfr.promise();
		}, this);

		var processRemoved = $.proxy(function(g){
			var dfr =  $.Deferred();
			var reqId;
			reqId = butor.sec.group.GroupAjax.readGroup(g, $.proxy(function(result){
				if (!AJAX.isSuccessWithData(result, reqId)) {
					dfr.reject();
					return;
				}
				var groupDetail = result.data[0];
				var items = groupDetail.items;
				groupDetail.items = $.grep(groupDetail.items, function(m, i){
					if (m.member == uid){
						return false;
					}
					return true;
				});
				tobeSaved.push(groupDetail);
				dfr.resolve();
			}, this));
			return dfr.promise();
		}, this);
			
		var defAdded = $.map(added, processAddded);
		var defRemoved = $.map(removed, processRemoved);
		var defs = $.merge(defAdded, defRemoved);
		
		$.when.apply($, defs).done($.proxy(function(){
			callback(tobeSaved);
		}, this)).fail(function(){
			return [];
		});
	},	
	_newUser : function(user) {
		this._isEdit = true;
		this._user = user;
		if (this._user === undefined) {
			this._user = {revNo:null};
		}
		this._showUserInfo();
		this._editMode();
	},
	_loadGroupList: function (callback){
		if (!this._canReadGroup){
			return;
		}
		var reqId = butor.sec.group.GroupAjax.listGroup(null, null, $.proxy(function(result){
			if (!AJAX.isSuccessWithData(result, reqId)) {
				return;
			}
			this._allGroups = result.data;
			reqId = butor.sec.group.GroupAjax.listGroup(this._user.id, null, $.proxy(function(result){
				if (!AJAX.isSuccessWithData(result, reqId)) {
					return;
				}
				this._myGroups = result.data;
				callback();
			},this));
		},this));
	},
	_renderGroupPanel: function (){
		var panel = this._jqe.find('#groupPanel');
		var tbody = panel.find('#resultTable > tbody');
		tbody.empty();
		for (var ii=0; ii<this._allGroups.length; ii++) {
			var group = this._allGroups[ii];
			butor.Utils.escape(group, butor.sec.group.fte);
			var row = $('<tr class="group" id="' + group.id + '" + data-group-id="' + group.id + '" revNo="' + group.revNo + '">');
			var div = $('<div class="checkbox">');
			var label = $('<label>'); 
			var checkbox = $('<input type="checkbox" class="editable" disabled id="' + group.id + '" data-group-id="' + group.id + '">');
			label.append(checkbox).append(group.description);
			div.append(label);
			row.append($('<td>').append(div));
			tbody.append(row);
		}
		$.each(this._myGroups, function(i, g){
			tbody.find('input[type=checkbox][data-group-id="' + g.id + '"]').prop('checked', true);
		});
	},
	_view : function(id, isEdit) {
		this._isEdit = isEdit;
		this._disableForm();
		this.hideMsg();
		this.mask(this.tr("Initializing"));
		this._reqId = butor.sec.user.UserAjax.readUser(id, null, {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				this._user = result.data[0];
				this._loadGroupList($.proxy(this._renderGroupPanel, this));
				this._showUserInfo();
				this._jqe.find("#id").removeClass('editable');
				if (isEdit) {
					this._editMode();
				}
		}});
	},
	_edit : function(id) {
		this._view(id, true);
	},
	_showUserInfo : function() {
		if (!this._user) {
			return;
		}
		this._jqe.find("#id").val(this._user.id);
		this._jqe.find("#email").val(this._user.email);
		this._jqe.find("#phone").val(this._user.phone);
		this._jqe.find("#firstName").val(this._user.firstName);
		this._jqe.find("#lastName").val(this._user.lastName);
		this._jqe.find("#displayName").val(this._user.displayName);
		this._jqe.find("#fullName").val(this._user.fullName);
		this._jqe.find("#firmId").val(this._user.firmId);
		this._jqe.find("#active").attr("checked", this._user.active);
		this._jqe.find("#locked").attr("checked", this._user.missedLogin >= 3);
		this._jqe.find("#resetInProgress").attr("checked", this._user.resetInProgress);

		if (this._user.pwdEditable) {
			this._jqe.find("td.pwd").removeClass('hidden');
		}
	},
	_confirmReset : function() {
		if (!confirm(this.tr('Are you sure you want to reset this user login?'))) {
			return;
		}
		this.mask(this.tr("Reseting"));
		this._reqId = butor.sec.firm.FirmAjax.readFirm(this._user.firmId, "sec", "firms", "READ", {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				var firm = result.data[0];
				if (firm.attributesMap && firm.attributesMap.allowedDomains && firm.attributesMap.allowedDomains != '*') {
					var rJqe = this._jqe.find('#resetOnDomain');
					rJqe.find('#domainsBlock').show();
					var dJqe = rJqe.find('#domains');
					dJqe.empty();
					rJqe.find('#resetLink').empty().hide();
					
					rJqe.find('.btn').show();
					this._jqe.find('#userPanel').mask('');
					this._jqe.find('#userPanel').find('.loadmask-msg').hide();

					rJqe.find('#cancelResetBtn').show();
					rJqe.find('#okResetBtn').hide();
					rJqe.find('#confirmResetBtn').show();

					rJqe.slideDown();
					
					var dl = firm.attributesMap.allowedDomains.split(',');
					for (var i=0; i<dl.length; i+=1) {
						var domain = dl[i];
						var op = this._domainTmpl.clone();
						op.find('input').attr('value', domain);
						op.find('.bundle').html(domain);
						if (i==0) {
							op.find('input').attr('checked', true);
						}
						op.appendTo(dJqe);
					}
				} else {
					this._resetLogin();
				}
		}});
	},
	_resetShow : function() {
		this._resetAndSendLink = false;
		this._confirmReset();
	},
	_resetSend : function() {
		this._resetAndSendLink = true;
		this._confirmReset();
	},
	_resetLogin : function() {
		//this.mask(this.tr("Reseting"));
		var url = document.location.protocol +'//';
		var onDomain = this._jqe.find('#resetOnDomain #domains input:checked').val();
		if (butor.Utils.isEmpty(onDomain)) {
			onDomain = document.location.host;
		}
		url += onDomain +'/sso';
		this._reqId = butor.sec.user.UserAjax.resetLogin(this._user.id, url, this._resetAndSendLink, {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				if (this._resetAndSendLink) {
					this._unmaskReset();
				} else {
					//show reset link
					var rJqe = this._jqe.find('#resetOnDomain');
					rJqe.find('#domainsBlock').hide();
					rJqe.find('#resetLink').empty()
						.append('<b>' +this.tr('Reset link') +'</b> : <a href="' +result.data[0] +
							'" onclick="return false;">' +
							this.tr('Copy this link') +'</a>').show();
					
					rJqe.find('.btn').hide();
					rJqe.find('#okResetBtn').show();
					rJqe.slideDown();
				}
				this.msgInfo(this.tr('Reset done'));
		}});
	},
	_unmaskReset : function() {
		this._jqe.find('#resetOnDomain').slideUp($.proxy(function() {
			this._jqe.find('#userPanel').unmask();
		}, this));
	}
});

butor.sec.user.UserAjax = butor.sec.user.UserAjax  || function() {
	return {
		listUser: function(args, func, handler) {
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:'listUser'},
				[args, func], handler);
		},
		readUser: function(id_, func, handler) {
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:'readUser'},
				[id_, func], handler);
		},
		deleteUser: function(user, handler) {
			if (!confirm(butor.sec.tr('Remove user') +' ' +user.id +'?')) {
				return null;
			}
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:'deleteUser'},
				[user], handler);
		},
		updateUser: function(args, handler) {
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:(!butor.Utils.isEmpty(args.revNo)?'updateUser':'insertUser')},
				[args], handler);
		},
		readLdapUser: function(id_, handler) {
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:'readLdapUser'},
				[id_], handler);
		},
		resetLogin: function(id, onDomain, resetAndSendLink, handler) {
			return AJAX.call('/bsec/user.ajax', 
				{streaming:false, service:'resetLogin'},
				[id, onDomain, resetAndSendLink], handler);
		}
	}
}();

butor.sec.user.UserLookup = butor.sec.user.UserLookup || butor.LiveSearch.extend({
	_typeaheadTimer : null,
	_foundItems : {},
	_jqeElm : null,
	_lReqId : null,
	_selected : null,
	_authFunc : null,
	tr : function(txt) {
		return butor.sec.tr(txt);
	},
	construct: function(jqeElm, authFunc) {
		this._authFunc = authFunc;
		this._jqeElm = jqeElm;
		
		this.base(jqeElm, 
				{'id':'id',
				'selectEvent':'user-selected'});

		this._jqeElm.attr('placeholder', this.tr('User lookup'));

		App.bind('langChanged', $.proxy(this._langChanged, this));
	},
	_langChanged : function() {
		this._jqeElm.attr('placeholder', this.tr('User lookup'));
	},
	search : function(text, callback) {
		var crit = {'displayName':text};		
		self._lReqId = butor.sec.user.UserAjax.listUser(crit, self._authFunc, function(result) {
			if (!AJAX.isSuccessWithData(result, self._lReqId)) {
				callback(null);
				return;
			}
			var data = result.data;
			for (var ii=0; ii<data.length; ii++) {
				var item = data[ii];
				item.label = item.id +' - ' +item.displayName;
			}
			callback(data);
		});

	},
	read : function(id, callback) {
		self._lReqId = butor.sec.user.UserAjax.readUser(id, this._authFunc, function(result) {
			if (!AJAX.isSuccessWithData(result, self._lReqId)) {
				callback(null);
				return;
			}
			var user = result.data[0];
			user.label = user.id +' - ' +user.displayName;
			callback(user);
		});
	}
});

butor.sec.user.UserLookupDlg = butor.sec.user.UserLookupDlg || butor.dlg.Dialog.define({
	statics : {
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("User lookup"),
				width : 650,
				height : 400,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.user.UserLookupDlg(dlgOpts);
			//var html = wiki._frags.find('#__acl_dlg__').clone().html();
			dlg.show(args);
			return dlg;
		}
	},
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/userLookupDlg.html');
		this._userFilter = null;
		this._usersTable = null;
		this._cancelBtn = null;
		this._okBtn = null;
		this._count = 0;
		this._cReqId = 0;
		this._selectedId = null;
		this._users = {};
		this._firms = {};
	},
	_init : function() {
		this.base();
		this._usersTable = this._jqe.find('#usersTable');

		this._reqIdFirm = butor.sec.firm.FirmAjax.listFirm({streaming : false, sys:"sec", func:"firms", mode:"READ"}, 
				{'scope':this,'callback':function(result) {
			if (!AJAX.isSuccessWithData(result, this._reqIdFirm)) {
				return;
			}
			var firms = result.data;
			for ( var cc = 0; cc < firms.length; cc++) {
				var firm = firms[cc];
				this._firms[firm.firmId] = butor.Utils.escapeStr(firm.firmName);
			}
		}});

		var self = this;
		this._userFilter = this._jqe.find('#userFilter').keypress(function(e) {
			if (e.keyCode == 13) {
				self._listUsers();
			}
		}).focus();
		this._goBtn = this._jqe.find('#goBtn').click(function() {
			self._listUsers();
		});

		this._cancelBtn = this._jqe.find('#cancelBtn').click(function() {
			self._onCancel();
		});

		this._okBtn = this._jqe.find('#okBtn').click(function() {
			self._onOk();
		});
	},
	_onCancel : function() {
		if (this._args.callback != null) {
			if (!this._args.callback.call(this._args.scope, null)) // cancelled
				return;
		}
		this.close();
	},
	_onOk : function() {
		if (this._args.callback != null) {
			var su = this._users[this._selectedId];
			if (!su) {
				return;
			}
			// wont return all fields
			var user = {"id":su.id,
				"firstName":su.firstName,
				"lastName":su.lastName,
				"displayName":su.displayName,
				"email":su.email,
				"firmId":su.firmId,
				"active":su.active};
			if (!this._args.callback.call(this._args.scope, user)) {
				return;
			}
		}
		this.close();
	},
	clear : function() {
		this.hideMsg();
		this._usersTable.find('tbody').empty();
		this._okBtn.butor('disable');
		this._users = {};
	},
	_listUsers : function() {
		this.clear();
		this._count = 0;
		this.hideMsg();
		var filter = this._userFilter.val();
		if (butor.Utils.isEmpty(filter)) {
			return;
		}
		var crit = {'displayName' : filter};
		this._cReqId = butor.sec.user.UserAjax.listUser(crit, null,
			{'msgPanel':this.getMsgPanel(), 'scope':this, 'callback':function(result) {
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}
				this._handleListUser(result.data);
			}
		});
	},
	_handleListUser : function(data_) {
		var tb = this._usersTable.find('tbody');
		for (var ii=0; ii<data_.length; ii++) {
			var user = data_[ii];
			
			butor.Utils.escape(user, butor.sec.user.fte);
			
			this._count++;
			//this.progress(_count, _totalRows, 'loading ' +_count +'/' +_totalRows);

			var row = $('<tr class="user" userId="' +user.id +'">');
			row.html("<td class=\"number\">" +this._count +"</td><td>" + user.id +
					"</td><td>" +user.displayName +"</td><td>" +
					(user.firmName ? user.firmName : this._firms[user.firmId]) +
					'</td><td class="align-center">' +(user.active ? "<i class=\"fa fa-check\"></i>" : '') +
					"</td>");

			this._users[user.id] = user;
			tb.append(row);
		}
		var self = this;
		tb.find('tr').css('cursor', 'pointer').click(function() {
			var row = $(this);
			tb.find('tr.active').removeClass('active');
			row.addClass('active');
			self._selectedId = row.attr('userId');
			self._okBtn.butor('enable');
		});
	}
});


butor.sec.group = butor.sec.group || {
	fte : ["id", "description"]
};

butor.sec.group.Groups = butor.sec.group.Groups || butor.Class.define({
	_selectedItem : null,
	_selectedItemFields : ["id", "revNo"],
	_reqId : null,
	_cReqId : null,
	_ctxMenu : null,
	_firms : {},
	_jqe : null,
	_userLookup : null,
	_tableCtxtMenu : null,
	_canUpdate : App.hasAccess('sec', 'groups', butor.sec.ACCESS_MODE_WRITE),
	statics : {
		//TODO move
		userGroups: function(member) {
			App.showPage('sec.groups', {member:member});
		}
	},
	construct : function(jqe) {
		this._jqe = jqe;

		this._userLookup = new butor.sec.user.UserLookup(this._jqe.find('#member'));
		this._userLookup.bind('user-selected', $.proxy(this._criteriaChanged, this));

		this._jqe.find('#resultTable').tablesorter();

		var listGroupP = $.proxy(this._listGroup, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			var dlg = butor.sec.group.GroupDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('group-updated', listGroupP);
			}
		}, this));
		if (this._canUpdate) {
			this._ctxMenu.find('li.edit').show().find('a').click($.proxy(function(e_) {
				var dlg = butor.sec.group.GroupDlg.edit(this._tableCtxtMenu.getHoveredItem().id);
				if (dlg) {
					dlg.bind('group-updated', listGroupP);
				}
			}, this));
			this._ctxMenu.find('li.delete').show().find('a').click($.proxy(function(e_) {
				var rReqId = butor.sec.group.GroupAjax.deleteGroup(this._tableCtxtMenu.getHoveredItem(), $.proxy(function(result){
					if (!AJAX.isSuccess(result, rReqId)) {
						return;
					}
					// refresh
					this._listGroup();
				}, this));
			}, this));
			this._jqe.find('#addBtn').show().click($.proxy(function(){
				var dlg = butor.sec.group.GroupDlg.newGroup();
				if (dlg) {
					dlg.bind('group-updated', listGroupP);
				}
			}, this));
		}
		this._ctxMenu.find('li.groups a').click($.proxy(function(e_) {
			butor.sec.group.Groups.userGroups(this._tableCtxtMenu.getHoveredItem().id);
		}, this));
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._jqe.find('#resultTable'), this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			if (butor.Utils.isEmpty(e.data.revNo)) {
				this._ctxMenu.find('li.view').hide();
				this._ctxMenu.find('li.edit').hide();
			} else {
				this._ctxMenu.find('li.view').show();
				if (this._canUpdate) {
					this._ctxMenu.find('li.edit').show();
				}
			}
		}, this));

		this._jqe.find('#goBtn').click(listGroupP);
		this._jqe.find('#resetBtn').click($.proxy(function(e_){
			this._userLookup.clear();
			this.clear();
		}, this));

		this._jqe.find(".form-inline").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listGroup();
				return false;
			}
		}, this));
		
		var pArgs = App.getPageArgs();
		if (pArgs && pArgs.member) {
			this._userLookup.select(pArgs.member);
			this._listGroup();
		}
		this._jqe.find("#resultTable").delegate('a.group-link', 'click', $.proxy(function(e_) {
			var dlg = butor.sec.group.GroupDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('group-updated', $.proxy(this._listGroup, this));
			}
		}, this));
	},
	_criteriaChanged : function() {
		//TODO later this.clear();
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		var tbody = this._jqe.find('#resultTable > tbody');
		tbody.empty();
	},
	_listGroup : function() {
		App.hideMsg();
		this.clear();
		var member = null;
		var user = this._userLookup.getSelection();
		if (user) {
			member = user.id;
		}
		this._cReqId = butor.sec.group.GroupAjax.listGroup(member, null, 
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}
				var data = result.data;
				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<data.length; ii++) {
					var group = data[ii];
					butor.Utils.escape(group, butor.sec.group.fte);
					var row = $("<tr class=\"group\" id=\"" +group.id +"\" revNo=\"" +group.revNo +"\">");
					row.html("<td class=\"number\">" +(ii+1) +"</td><td>" +group.id +"</td><td><a class=\"group-link\">" +group.description +"</a></td>" +
							"<td>" +butor.Utils.formatDateTime(group.stamp) +"</td><td>" +group.userId +"</td>");

					tbody.append(row);
				}
				this._jqe.find("#resultTable").trigger("update");
			}, this));
	}
});

butor.sec.group.GroupAjax = butor.sec.group.GroupAjax || function() {
	return {
		listGroup: function(member, func, handler) {
			return AJAX.call('/bsec/group.ajax', 
				{streaming:false, service:'listGroup'},
				[member, func], handler);
		},
		readGroup: function(id_, handler) {
			return AJAX.call('/bsec/group.ajax', 
				{streaming:false, service:'readGroup'},
				[id_], handler);
		},
		updateGroup: function(args, handler) {
			return AJAX.call('/bsec/group.ajax', 
				{streaming:false, service:(!butor.Utils.isEmpty(args.revNo) ? 'updateGroup' : 'createGroup')},
				[args], handler);
		},
		deleteGroup: function(groupKey_, handler) {
			return AJAX.call('/bsec/group.ajax', 
				{streaming:false, service:'deleteGroup'},
				[groupKey_], handler);
		}
	}
}();

butor.sec.group.GroupDlg = butor.sec.group.GroupDlg || butor.dlg.Dialog.define({
	statics : {
		newGroup : function(group) {
			var args = {'group':group, 'mode':'add'};
			return butor.sec.group.GroupDlg.show(args);
		},
		view : function(id) {
			var args = {'id':id, 'mode':'view'};
			return butor.sec.group.GroupDlg.show(args);
		},
		edit : function(id) {
			var args = {'id':id, 'mode':'edit'};
			return butor.sec.group.GroupDlg.show(args);
		},
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("Group"),
				width : 800,
				height : 400,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.group.GroupDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_group : null,
	_reqId : null,
	_isEdit : false,
	_canUpdate : App.hasAccess('sec', 'groups', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/groupDlg.html');
	},
	_init : function() {
		this.base();
		App.translateElem(this._jqe, 'sec');
		this._usersCmp = new butor.sec.user.AuthDataUserEditor(
				{'elem':this._jqe.find('#userSelPane'), 
					'msgPanel':this.getMsgPanel(),
					'showAllOption' : false});
		this.bind('resized', $.proxy(function() {
			this._resized();
		}, this));

		this._usersCmp.bind('ready', $.proxy(function() {
			App.translateElem(this._jqe, 'sec');
			switch (this._args.mode) {
			case 'add':
				this._newGroup();
				break;
			case 'view':
				this._view(this._args.id);
				break;
			case 'edit':
				this._edit(this._args.id);
				break;
			}
		}, this));

		this._jqe.find('#cancelBtn').click($.proxy(function() {
			if (this._isEdit)
				this.close();
			else
				this._disableForm();
		}, this));
		this._jqe.find('#closeBtn').click($.proxy(this.close, this));
		this._jqe.find('#saveBtn').click($.proxy(this._save, this));
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').click($.proxy(this._editMode, this));
		}
		this._resized();
	},
	_resized : function() {
		var top = this._jqe.find('#idPane').height();
		this._jqe.find('#userSelPane').css('top', top);
	},
	_editMode : function() {
		this._usersCmp.enable();
		this._jqe.find("#groupId.editable").butor('enable');
		this._jqe.find("#description").butor('enable');

		this._jqe.find('#closeBtn').hide();
		this._jqe.find('#modifyLink').hide();
		this._jqe.find('#cancelBtn').show();
		this._jqe.find('#saveBtn').show();
		this.hideMsg();
	},
	_disableForm : function() {
		this._usersCmp.disable();
		this._jqe.find("#groupId").butor('disable');
		this._jqe.find("#description").butor('disable');

		this._jqe.find("#saveBtn").hide();
		this._jqe.find("#cancelBtn").hide();
		this._jqe.find('#closeBtn').show();
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').show();
		}
		this._showGroupInfo();
	},
	_save : function() {
		var args = this._getData();
		this.mask(this.tr("Saving"));
		this.hideMsg();
		this._reqId = butor.sec.group.GroupAjax.updateGroup(args, {'msgHandler':this.getMsgPanel(), 
			'scope':this, 'callBack':function(result) {
			this.unmask();
			if (!AJAX.isSuccessWithData(result, this._reqId)) {
				return;
			}
			this._view(args.id);
			this.showMsg(this.tr('Saved'));
			this.fire('group-updated', this._group.id);
		}});
	},
	_getData : function() {
		var data = this._group;
		if (!data.revNo) {
			data.id = this._jqe.find("#groupId").val();
		}
		data.description = this._jqe.find("#description").val();
		data.items = [];
		var users = this._usersCmp.getUsers();
		for (var i=0,c=users.length; i<c; i+=1) {
			data.items.push({'member' : users[i]});
		}
		return data;
	},
	_newGroup : function() {
		this._isEdit = true;
		this._group = {revNo:null};
		this._jqe.find('#saveBtn').show();
		this._editMode();
	},
	_view : function(id, isEdit) {
		this._isEdit = isEdit;
		this._disableForm();
		this.hideMsg();
		this.mask(this.tr("Initializing"));
		this._reqId = butor.sec.group.GroupAjax.readGroup(id, {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				this._group = result.data[0];
				butor.Utils.escape(this._group, butor.sec.group.fte);
				this._showGroupInfo();
				this._jqe.find("#id").removeClass('editable');
				if (isEdit) {
					this._editMode();
				}
		}});
	},
	_edit : function(id) {
		this._view(id, true);
	},
	_showGroupInfo : function() {
		this._jqe.find("#groupId").val('');
		this._jqe.find("#description").val('');
		if (!this._group) {
			return;
		}
		this._jqe.find("#groupId").val(this._group.id).removeClass('editable');
		this._jqe.find("#description").val(this._group.description);

		var items = this._group.items;
		var users = [];
		for (var i=0,c=items.length; i<c; i+=1) {
			users.push(items[i].member);
		}
		this._usersCmp.setUsers(users);
	}
});

butor.sec.group.AuthDataGroupEditor = butor.sec.group.AuthDataGroupEditor || butor.sec.auth.AuthDataBaseEditor.extend({
	_reqId : null,
	tr : butor.sec.tr,

	construct : function(args) {
		this._jqe = args.elem;
		args.prefix = 'adge-';
		args.sys = 'sec';
		args.htmlFile = '/bsec/authDataGroupEditor.html';
		this.base(args);
	},
	_loadItems : function(callback) {
		this._reqId = butor.sec.group.GroupAjax.listGroup(null, "auths", {msgHandler:this._msgPanel, 
			'scope':this, callBack:function(result) {
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					callback(null);
					return;
				}
				callback(result.data);
			}
		});
	},
	_formatItem : function(key) {
		var group = this._getItem(key);
		return (group ? group.description : key);
	},
	getData : function() {
		var groups = this._getSelectedKeys();
		var data = [];
		for (var i=0,c=groups.length; i<c; i+=1) {
			data.push({'sys':'sec','dataType':'group','d1':groups[i]});
		}
		return data;
	},
	setData : function(data) {
		var groups = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				groups.push(data[i].d1);
			}
		}
		this._setSelectedKeys(groups);
	}
});
if (!butor.sec.auth.getDataEditorNames()['group']) {
	butor.sec.auth.registerFactory('group', 'Group', function(args) {
		return new butor.sec.group.AuthDataGroupEditor(args);
	});
}

butor.sec.func = butor.sec.func || {};

butor.sec.func.Funcs = butor.sec.func.Funcs || butor.Class.define({
	_selectedId : null,
	_sReqId : null,
	_cReqId : null,
	_sysCs : null,
	_jqe : null,
	construct : function(jqe) {
		this._jqe = jqe;

		this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'funcs',
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._sReqId)) {
					return;
				}
				this._sysCs = App.AttrSet.createHelper(result.data);
				App.Utils.fillSelect(this._jqe.find('#sysSB'), result.data, {showEmpty:true});
			}, this));

		this._jqe.find('#resultTable').tablesorter();

		var listFuncsP = $.proxy(this._listFuncs, this);
		this._jqe.find('#goBtn').click(listFuncsP);
		this._jqe.find('#resetBtn').click($.proxy(function(e_){
			this._jqe.find('#func').val('');
			this._jqe.find('#sysSB').val('');
			this.clear();
		}, this));
		this._jqe.find('#sysSB').change(listFuncsP);
		this._jqe.find('#func').change(listFuncsP);

		this._jqe.find(".form-inline").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listFuncs();
				return false;
			}
		}, this));
	},
	clear : function() {
		var tbody = this._jqe.find('#resultTable > tbody');
		tbody.empty();
	},
	_listFuncs : function() {
		App.hideMsg();
		this.clear();
		var crit = {};
		var sys = this._jqe.find('#sysSB').val();
		if (butor.Utils.isEmpty(sys)) {
			sys = null;
		}
		this._cReqId = butor.sec.func.FuncAjax.listFunc(sys, null,
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}
				var re = null;
				var desc = this._jqe.find('#func').val();
				if (desc) {
					re = new RegExp(desc, "gi");
				}
				var data = result.data;
				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<data.length; ii++) {
					var func = data[ii];
					var row = $("<tr class=\"func\" id=\"" +func.func +"\">");
					row.html("<td class=\"number\">" +(ii +1) +"</td><td>" +this._sysCs.get(func.sys) +"</td><td>" +func.func +"</td>" +
							"<td class=\"desc\">" +func.description +"</td>");

					if (re && !func.description.match(re)) {
						row.hide();
					}
					tbody.append(row);
				}
				this._jqe.find("#resultTable").trigger("update");
				if (re) {
					App.showMsg(this.tr('Filter was applied'));
				}
			}, this));
	}/*,
	_filter : function() {
		var desc = this._jqe.find('#func').val();
		var re = null;
		if (desc) {
			re = new RegExp(desc, "gi");
		}
		this._jqe.find('#resultTable > tbody > tr').each(function(index, elem) {
			var row = $(this);
			var desc = row.find('td.desc').text();
			if (re && !desc.match(re)) {
				row.hide();
			} else {
				row.show();
			}
		});
	}*/
});

butor.sec.func.FuncAjax = butor.sec.func.FuncAjax || function() {
	return {
		listFunc: function(sys, func, handler) {
			return AJAX.call('/bsec/func.ajax', 
				{streaming:false, service:'listFunc'},
				[sys, func], handler);
		},
		readFunc: function(id_, handler) {
			return AJAX.call('/bsec/func.ajax', 
				{streaming:false, service:'readUser'},
				[id_], handler);
		},
		updateFunc: function(args, handler) {
			return AJAX.call('/bsec/func.ajax', 
				{streaming:false, service:(butor.Utils.isEmpty(args.revNo)?'updateFunc':'insertFunc')},
				[args], handler);
		}
	}
}();

butor.sec.func.AuthDataFuncEditor = butor.sec.func.AuthDataFuncEditor || butor.sec.auth.AuthDataBaseEditor.extend({
	_reqId : null,
	_reqId2 : null,
	_sysSB : null,
	_funcsSB : null,
	_sysCs : null,
	_allFuncs : null,
	tr : butor.sec.tr,

	construct : function(args) {
		this._jqe = args.elem;
		args.prefix = 'adfunce-';
		args.sys = 'sec';
		args.htmlFile = '/bsec/authDataFuncEditor.html';
		this.base(args);
	},
	_ready : function() {
		if (this._allFuncs) {
			//this._showAvailItems();
			this._isReady = true;
			this.fire('ready', null);
		}
	},
	_clearItems : function() {
		this.base();
		this._sysSB.val('').change();
	},
	_loadItems : function(callback) {
		//if (this._sysSB) {
		//	return;
		//}
		this._funcsSB = this._jqe.find('#adfunce-itemsSB');
		this._sysSB = this._jqe.find('#adfunce-sysSB').change($.proxy(this._listFuncs, this));
		this._reqId2 = App.AttrSet.getCodeSet({id:"systems", lang:App.getLang()},
			$.proxy(function(result) {
				if (result.reqId != this._reqId2 || !result.data || !result.data[0]) {
					return;
				}
				this._sysCs = App.AttrSet.createHelper(result.data[0]);
				App.Utils.fillSelect(this._sysSB, result.data[0], {showEmpty:true});
			}, this)
		);
		
		this._reqId3 = butor.sec.func.FuncAjax.listFunc(null, "auths", $.proxy(function(result) {
			if (!AJAX.isSuccessWithData(result, this._reqId3)) {
				return;
			}
			this._allFuncs = {};
			var data = result.data;
			for (var ii=0; ii<data.length; ii++) {
				var item = data[ii];
				var key = item.sys +';*';
				if (!this._allFuncs[key]) {
					this._allFuncs[key] = {'id':key, 
						'sys':item.sys, 'func':'*', 'description':'(' +this.tr('All functions') +')'};
				}
				item.id = item.sys +';' +item.func;
				this._allFuncs[item.id] = item;
			}
			this._ready();
		}, this));
		callback(null); // will build items list ourselves
	},
	_listFuncs : function() {
		this._funcsSB.empty();
		this._availableItems = {};
		var sys = this._sysSB.val();
		if (butor.Utils.isEmpty(sys)) {
			return;
		}
		for (var key in this._allFuncs) {
			var item = this._allFuncs[key];
			if (item.sys == sys) {
				this._availableItems[item.id] = item;
				//this._funcsSB.append('<option value="' +item.id +'">' +item.description +'</option>');
				this._funcsSB.append('<div class="checkbox"><label> <input value="' +item.id +'" type="checkbox">' +item.description +'</label></div>');
			}
		}
	},
	_formatItem : function(key) {
		var func = this._allFuncs[key];
		if (func) {
			return func.description || func.func;
		} else {
			return key;
		}
	},
	_formatSelectedItem : function(key) {
		var func = this._allFuncs[key];
		if (!func) {
			return key;
		}
		var sys = func.sys;
		var sysDesc = this._sysCs.get(sys) || sys;
		return sysDesc +' - ' +func.description;
	},
	_recycleItem : function(key) {
		var func = this._allFuncs[key];
		if (!func) {
			return;
		}
		if (func.sys != this._sysSB.val()) {
			return;
		}
		//var option = '<option value="' +func.id +'">' +this._formatItem(key) +'</option>';
		var option = '<div class="checkbox"><label> <input value="' +func.id +'" type="checkbox">' +this._formatItem(key) +'</label></div>';
		if (func.func == '*') {
			this._funcsSB.prepend(option);
		} else {
			this._funcsSB.append(option);
		}
	},
	getData : function() {
		var keys = this._getSelectedKeys();
		var data = [];
		if (keys.length == 1 && keys[0] == '*') {
			data.push({'sys':'sec','dataType':'func','d1':'*','d2':'*'});
			return data;
		}
		for (var i=0,c=keys.length; i<c; i+=1) {
			var func = keys[i].split(';');
			data.push({'sys':'sec','dataType':'func','d1':func[0],'d2':func[1]});
		}
		return data;
	},
	setData : function(data) {
		var funcs = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				if (data[i].d1 == '*' && data[i].d2 == '*') {
					funcs.push('*');
				} else {
					funcs.push(data[i].d1 +';' +data[i].d2);
				}
			}
		}
		this._setSelectedKeys(funcs);
	}
});
if (!butor.sec.auth.getDataEditorNames()['func']) {
	butor.sec.auth.registerFactory('func', 'System & Function', function(args) {
		return new butor.sec.func.AuthDataFuncEditor(args);
	});
}


butor.sec.role = butor.sec.role || {
	'fte' : ["id", "description"]
};

butor.sec.role.Roles = butor.sec.role.Roles || butor.Class.define({
	_selectedItem : null,
	_selectedItemFields : ['id', 'revNo'],
	_reqId : null,
	_sReqId : null,
	_fReqId : null,
	_ctxMenu : null,
	_jqe : null,
	_tableCtxtMenu : null,
	_canUpdate : App.hasAccess('sec', 'roles', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	construct : function(jqe) {
		this._jqe = jqe;
		this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'roles',
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._sReqId)) {
					return;
				}
				this._sysCs = App.AttrSet.createHelper(result.data);
				App.Utils.fillSelect(this._jqe.find('#sysSB'), result.data, {showEmpty:true});
			}, this));

		this._jqe.find('#resultTable').tablesorter();

		var listRolesP = $.proxy(this._listRoles, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			var dlg = butor.sec.role.RoleDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('role-updated', listRolesP);
			}
		}, this));
		if (this._canUpdate) {
			this._ctxMenu.find('li.edit').show().find('a').click($.proxy(function(e_) {
				var dlg = butor.sec.role.RoleDlg.edit(this._tableCtxtMenu.getHoveredItem().id);
				if (dlg) {
					dlg.bind('role-updated', listRolesP);
				}
			}, this));
			this._ctxMenu.find('li.delete').show().find('a').click($.proxy(function(e_) {
				var rReqId = butor.sec.role.RoleAjax.deleteRole(this._tableCtxtMenu.getHoveredItem(), $.proxy(function(result){
					if (!AJAX.isSuccessWithData(result, this._rReqId)) {
						return;
					}
					// refresh
					this._listRoles();
				}, this));
			}, this));
			this._jqe.find('#addBtn').show().click($.proxy(function(){
				var dlg = butor.sec.role.RoleDlg.newRole();
				if (dlg) {
					dlg.bind('role-updated', listRolesP);
				}
			}, this));
		}
		this._ctxMenu.find('li.groups a').click($.proxy(function(e_) {
			butor.sec.group.Groups.userGroups(this._tableCtxtMenu.getHoveredItem().id);
		}, this));
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._jqe.find('#resultTable'), this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			if (butor.Utils.isEmpty(e.data.revNo)) {
				this._ctxMenu.find('li.view').hide();
				this._ctxMenu.find('li.edit').hide();
			} else {
				this._ctxMenu.find('li.view').show();
				if (this._canUpdate) {
					this._ctxMenu.find('li.edit').show();
				}
			}
		}, this));

		this._jqe.find('#goBtn').click(listRolesP);
		this._jqe.find('#resetBtn').click($.proxy(function(e_){
			this._jqe.find('#sysSB').val('');
			this._jqe.find('#funcSB').empty();
			this.clear();
		}, this));
		
		this._jqe.find('#sysSB').change($.proxy(this._listFuncs, this));
		this._jqe.find('#funcSB').change(listRolesP);

		this._jqe.find(".form-inline").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listRoles();
				return false;
			}
		}, this));
		this._jqe.find("#resultTable").delegate('a.role-link', 'click', $.proxy(function(e_) {
			var dlg = butor.sec.role.RoleDlg.view(this._tableCtxtMenu.getHoveredItem().id);
			if (dlg) {
				dlg.bind('role-updated', $.proxy(this._listRoles, this));
			}
		}, this));
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		var tbody = this._jqe.find('#resultTable > tbody');
		tbody.empty();
	},
	_listFuncs : function() {
		App.hideMsg();
		var sb = this._jqe.find('#funcSB');
		sb.empty();
		sb.append("<option value=\"\"></option>");
		var sys = this._jqe.find('#sysSB').val();
		if (sys == '')
			return;

		App.mask(this.tr('Working'));
		this._fReqId = butor.sec.func.FuncAjax.listFunc(sys, "auths", $.proxy(function(result) {
			App.unmask();
			if (!AJAX.isSuccessWithData(result, this._fReqId)) {
				return;
			}
			var data = result.data;
			for (var ii=0; ii<data.length; ii++) {
				var item = data[ii];
				sb.append("<option value=\"" +item.func +"\">" +butor.Utils.escapeStr(item.description) +"</option>");
			}
		}, this));
	},
	_listRoles : function() {
		App.hideMsg();
		this.clear();
		var funcKey = {};
		var val = this._jqe.find('#sysSB').val();
		if (!butor.Utils.isEmpty(val))
			funcKey.sys = val;

		var val = this._jqe.find('#funcSB').val();
		if (!butor.Utils.isEmpty(val))
			funcKey.func = val;

		App.mask(this.tr('Working'));
		this._cReqId = butor.sec.role.RoleAjax.listRole(funcKey, null,
			$.proxy(function(result) {
				App.unmask();
				if (!AJAX.isSuccessWithData(result, this._cReqId)) {
					return;
				}
				var data = result.data;
				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<data.length; ii++) {
					var role = data[ii];
					butor.Utils.escape(role, butor.sec.role.fte);
					var row = $("<tr class=\"role\" id=\"" +role.id +"\" revNo=\"" +role.revNo +"\">");
					row.html("<td class=\"number\">" +(ii +1) +"</td><td>" +role.id +"</td><td><a class=\"role-link\">" +role.description +"</a></td>" +
							"<td>" +butor.Utils.formatDateTime(role.stamp) +"</td><td>" +role.userId +"</td>");

					tbody.append(row);
				}
				this._jqe.find("#resultTable").trigger("update");
			}, this));
	}
});

butor.sec.role.RoleDlg = butor.sec.role.RoleDlg || butor.dlg.Dialog.define({
	statics : {
		newRole : function(user) {
			var args = {'user':user, 'mode':'add'};
			return butor.sec.role.RoleDlg.show(args);
		},
		view : function(id) {
			var args = {'id':id, 'mode':'view'};
			return butor.sec.role.RoleDlg.show(args);
		},
		edit : function(id) {
			var args = {'id':id, 'mode':'edit'};
			return butor.sec.role.RoleDlg.show(args);
		},
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("Role"),
				width : 800,
				height : 420,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.role.RoleDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_role : null,
	_reqId : null,
	_isEdit : false,
	_roles : {},
	_sysCs : null,
	_roleFuncsMap : {},
	_iReqId : false,
	_sReqId : null,
	_canUpdate : App.hasAccess('sec', 'roles', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/roleDlg.html');
	},
	_init : function() {
		this.base();
		App.translateElem(this._jqe, 'sec');
		this._modeSBTmpl = this._jqe.find('#modeSBTmpl').remove();
		this._jqe.find('#sysSB').change($.proxy(this._listFuncs, this));

		this._jqe.find('#cancelBtn').click($.proxy(function() {
			if (this._isEdit)
				this.close();
			else
				this._disableForm();
		}, this));
		this._jqe.find('#closeBtn').click($.proxy(this.close, this));
		this._jqe.find('#saveBtn').click($.proxy(this._save, this));
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').click($.proxy(this._editMode, this));
		}

		this._jqe.find('#removeBtn').click($.proxy(this._removeFuncs, this)).attr('title', this.tr('Remove'));
		this._jqe.find('#addBtn').click($.proxy(this._addFuncs, this)).attr('title', this.tr('Add'));
		
		var task1 = $.proxy(function() {
			var dfr =  $.Deferred();
			this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'roles',
				$.proxy(function(result) {
					if (!AJAX.isSuccessWithData(result, this._sReqId)) {
						return;
					}
					this._sysCs = App.AttrSet.createHelper(result.data);
					App.Utils.fillSelect(this._jqe.find('#sysSB'), result.data, {showEmpty:true});
					dfr.resolve();
				}, this));
			return dfr.promise();
		}, this);
		
		var task2 = $.proxy(function() {
			var dfr =  $.Deferred();
			this._iReqId = App.AttrSet.getCodeSets([{id:"auth-mode", lang:App.getLang()}],
				$.proxy(function(result) {
					if (!AJAX.isSuccessWithData(result, this._iReqId)) {
						dfr.reject();
						return;
					}
					this._accessModeCs = App.AttrSet.createHelper(result.data[0]);
					App.Utils.fillSelect(this._modeSBTmpl, result.data[0], {showEmpty:false});
					dfr.resolve();
				}, this));
			return dfr.promise();
		}, this);
		
		$.when(task1(), task2()).always($.proxy(function() {
			App.unmask();
			switch (this._args.mode) {
			case 'add':
				this._newRole(this._args.role);
				break;
			case 'view':
				this._view(this._args.id);
				break;
			case 'edit':
				this._edit(this._args.id);
				break;
			}
		}, this)).fail(function() {
			//ok
		});
		this.bind('resized', $.proxy(this._resized, this));
		this._resized();
	},
	_resized : function() {
		var top = this._jqe.find('#idPane').height();
		this._jqe.find('#roleSelPane').css('top', top);
	},
	_listFuncs : function() {
		App.hideMsg();
		var sb = this._jqe.find('#funcsSB');
		sb.empty();
		var sys = this._jqe.find('#sysSB').val();
		if (sys == '')
			return;

		App.mask(this.tr('Working'));
		this._fReqId = butor.sec.func.FuncAjax.listFunc(sys, "auths", $.proxy(function(result) {
			App.unmask();
			if (!AJAX.isSuccessWithData(result, this._fReqId)) {
				return;
			}
			var data = result.data;
			for (var ii=0; ii<data.length; ii++) {
				var item = data[ii];
				if (this._roleFuncsMap[item.sys +';' +item.func]) {
					// already in role
					continue;
				}
				var item = $('<div class="checkbox" style="white-space: nowrap;"><label> <input value="' +
						item.func +'" type="checkbox">' +item.description +'</label> :&nbsp;</div>');
				var modeSB = this._modeSBTmpl.clone().show();
				modeSB.appendTo(item);
				sb.append(item);
			}
		}, this));
	},
	_editMode : function() {
		this._jqe.find('.editable').butor('enable');
		this._jqe.find('input[type=checkbox]').butor('enable');
		this._jqe.find('#closeBtn').hide();
		this._jqe.find('#modifyLink').hide();
		this._jqe.find('#cancelBtn').show();
		this._jqe.find('#saveBtn').show();
		this.hideMsg();
	},
	_createOption : function(sys, func, desc, mode, enabled) {
		var html = '<div class="checkbox"><label> <input value="' +func +'" type="checkbox">';
		if (sys) {
			html += this._sysCs.get(sys) +' - ';
		}
		html += desc +'</label> : </div>';

		var opt = $(html);
		var chk = opt.find('input').butor(enabled ? 'enable' : 'disable');
		var modeSB = this._modeSBTmpl.clone().show().
				butor(enabled ? 'enable' : 'disable').
				appendTo(opt);
		if (mode != null) {
			modeSB.val(mode);
		}
		chk.attr({
			'desc':desc,
			'func':func,
			'sys':sys,
			'mode':mode});

		return opt;
	},
	_addFuncs : function() {
		var sys = this._jqe.find('#sysSB').val();
		var self = this;
		this._jqe.find('#funcsSB input:checked').each(function(index, elem) {
			var chk = $(this);
			var item = chk.parent().parent();
			var mode = item.find('select').val();
			var func = chk.attr('value');
			
			var opt = self._createOption(sys, func, chk.parent().text(), mode, true); 
			
			self._jqe.find('#roleFuncsSB').append(opt);
			item.remove();
			self._roleFuncsMap[sys +';' +func] = {'sys':sys, 'func':func, 'mode':mode};
		});
	},
	_removeFuncs : function() {
		var sys = this._jqe.find('#sysSB').val();
		var self = this;
		var sb = self._jqe.find('#funcsSB');
		this._jqe.find('#roleFuncsSB input:checked').each(function(index, elem) {
			var chk = $(this);
			var item = $(this).parent().parent();
			var func = chk.attr('value');
			var fsys = chk.attr('sys');
			if (sys == fsys) {
				self._createOption(sys, func, chk.attr('desc'), null, true).appendTo(sb);
			}
			item.remove();
			delete self._roleFuncsMap[fsys +';' +func];
		});
	},
	_disableForm : function() {
		this._jqe.find('.editable').butor('disable');
		this._jqe.find('input[type=checkbox]').butor('disable');
		this._jqe.find('#funcsSB').empty();
		this._jqe.find("#saveBtn").hide();
		this._jqe.find("#cancelBtn").hide();
		this._jqe.find('#closeBtn').show();
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').show();
		}
		this._showRoleInfo();
	},
	_save : function() {
		var args = this._getData();
		this.mask(this.tr("Saving"));
		this.hideMsg();
		this._reqId = butor.sec.role.RoleAjax.updateRole(args, {'msgHandler':this.getMsgPanel(), 
			'scope':this, 'callBack':function(result) {
			this.unmask();
			if (!AJAX.isSuccessWithData(result, this._reqId)) {
				return;
			}
			this._view(args.id);
			this.showMsg(this.tr('Saved'));
			this.fire('role-updated', this._role.id);
		}});
	},
	_getData : function() {
		var data = this._role;
		if (!data.revNo) {
			data.id = this._jqe.find("#roleId").val();
		}
		data.description = this._jqe.find("#description").val();
		data.items = [];
		this._jqe.find('#roleFuncsSB input').each(function(index, elem) {
			var chk = $(this);
			var item = $(this).parent().parent();
			var info = {'func':chk.attr('value'),
				'sys':chk.attr('sys'),
				'mode':item.find('select').val()}
			data.items.push(info);
		});
		return data;
	},
	_newRole : function(role) {
		this._isEdit = true;
		this._role = role;
		if (this._role === undefined) {
			this._role = {revNo:null};
		}
		this._editMode();
	},
	_view : function(id, isEdit) {
		this._isEdit = isEdit;
		this._disableForm();
		this.hideMsg();
		this.mask(this.tr("Initializing"));
		this._reqId = butor.sec.role.RoleAjax.readRole(id, {'msgHandler':this.getMsgPanel(),'scope':this, 
			'callBack':function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				this._role = result.data[0];
				this._showRoleInfo();
				this._jqe.find("#id").removeClass('editable');
				if (isEdit) {
					this._editMode();
				}
		}});
	},
	_edit : function(id) {
		this._view(id, true);
	},
	_showRoleInfo : function() {
		this._jqe.find('#funcsSB').empty();
		this._jqe.find('#sysSB').val('');
		this._jqe.find("#roleId").val('');
		this._jqe.find("#description").val('');
		this._roleFuncsMap = {};
		if (!this._role) {
			return;
		}
		this._jqe.find("#roleId").val(this._role.id).removeClass('editable');
		this._jqe.find("#description").val(this._role.description);

		var items = this._role.items || [];
		var sb = this._jqe.find('#roleFuncsSB');
		sb.empty();
		for (var ii=0; ii<items.length; ii++) {
			var item = items[ii];

			this._createOption(item.sys, item.func, item.description, item.mode, this._isEdit).appendTo(sb);
			var info = {'desc':item.description,
					'func':item.func,
					'sys':item.sys,
					'mode':item.mode};
			this._roleFuncsMap[item.sys +';' +item.func] = info;
		}
	}
});

butor.sec.role.AuthDataRoleEditor = butor.sec.role.AuthDataRoleEditor || butor.sec.auth.AuthDataBaseEditor.extend({
	_reqId : null,
	tr : butor.sec.tr,

	construct : function(args) {
		this._jqe = args.elem;
		args.prefix = 'adre-';
		args.sys = 'sec';
		args.htmlFile = '/bsec/authDataRoleEditor.html';
		this.base(args);
	},
	_loadItems : function(callback) {
		//{sys:"sec", func:"roles"}
		this._reqId = butor.sec.role.RoleAjax.listRole(null, "auths", {msgHandler:this._msgPanel, 
			'scope':this, callBack:function(result) {
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					callback(null);
					return;
				}
				callback(result.data);
			}
		});
	},
	_formatItem : function(key) {
		var role = this._getItem(key);
		return (role ? role.description : key);
	},
	getData : function() {
		var roles = this._getSelectedKeys();
		var data = [];
		for (var i=0,c=roles.length; i<c; i+=1) {
			data.push({'sys':'sec','dataType':'role','d1':roles[i]});
		}
		return data;
	},
	setData : function(data) {
		var roles = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				roles.push(data[i].d1);
			}
		}
		this._setSelectedKeys(roles);
	}
});
if (!butor.sec.auth.getDataEditorNames()['role']) {
	butor.sec.auth.registerFactory('role', 'Role', function(args) {
		return new butor.sec.role.AuthDataRoleEditor(args);
	});
}

butor.sec.role.RoleAjax = butor.sec.role.RoleAjax  || function() {
	return {
		listRole: function(criteria, func, handler) {
			return AJAX.call('/bsec/role.ajax', 
				{streaming:false, service:'listRole'},
				[criteria, func], handler);
		},
		readRole: function(id_, handler) {
			return AJAX.call('/bsec/role.ajax', 
				{streaming:false, service:'readRole'},
				[id_], handler);
		},
		updateRole: function(args, handler) {
			return AJAX.call('/bsec/role.ajax', 
				{streaming:false, service:(!butor.Utils.isEmpty(args.revNo) ? 'updateRole' : 'createRole')},
				[args], handler);
		},
		deleteRole: function(roleKey_, handler) {
			return AJAX.call('/bsec/role.ajax', 
				{streaming:false, service:'deleteRole'},
				[roleKey_], handler);
		}
	}
}();

butor.sec.auth.AuthList = butor.sec.auth.AuthList || butor.Class.define({
	_selectedItem : null,
	_selectedItemFields : ["authId", "revNo", "who", "what"],
	_reqId : null,
	_reqId2 : null,
	_fReqId : null,
	_ctxMenu : null,
	_jqe : null,
	_tableCtxtMenu : null,
	_roles : null,
	_groups : null,
	formatDate : butor.Utils.formatDate,
	formatDateTime : butor.Utils.formatDateTime,
	tr : butor.sec.tr,
	_whoTypeCS : null,
	_whatTypeCs : null,
	_sysCs : null,
	_accessModeCs : null,
	_canUpdate : App.hasAccess('sec', 'auths', butor.sec.ACCESS_MODE_WRITE),
	construct : function(jqe) {
		this._jqe = jqe;

		this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'auths',
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._sReqId)) {
					return;
				}
				this._sysCs = App.AttrSet.createHelper(result.data);
				App.Utils.fillSelect(this._jqe.find('#sysSB'), result.data, {showEmpty:true});
			}, this));

		this._reqId = App.AttrSet.getCodeSets([{id:"auth-who-type", lang:App.getLang()},
			{id:"auth-what-type", lang:App.getLang()},
			{id:"auth-mode", lang:App.getLang()}],
			$.proxy(function(result) {
				if (!result || result.hasError || !result.data || result.reqId != this._reqId) {
					return;
				}
				this._whoTypeCS = App.AttrSet.createHelper(result.data[0]);
				this._whatTypeCs = App.AttrSet.createHelper(result.data[1]);
				this._accessModeCs = App.AttrSet.createHelper(result.data[2]);
			}, this));

		this._rReqId = butor.sec.role.RoleAjax.listRole(null, "auths", $.proxy(function(result) {
			if (!AJAX.isSuccessWithData(result, this._rReqId)) {
				return;
			}
			var sb = this._jqe.find('#roleSB');
			sb.empty();
			sb.append("<option value=\"\"></option>");
			var data = result.data;
			this._roles = [];
			for (var ii=0; ii<data.length; ii++) {
				var role = data[ii];
				butor.Utils.escape(role, butor.sec.role.fte);
				this._roles[role.id] = role.description;
				sb.append("<option value=\"" +role.id +"\">" +role.description +"</option>");
			}
		}, this));

		this._gReqId = butor.sec.group.GroupAjax.listGroup(null, "auths", $.proxy(function(result) {
			if (!AJAX.isSuccessWithData(result, this._gReqId)) {
				return;
			}
			var sb = this._jqe.find('#groupSB');
			sb.empty();
			sb.append("<option value=\"\"></option>");
			var data = result.data;
			this._groups = [];
			for (var ii=0; ii<data.length; ii++) {
				var grp = data[ii];
				butor.Utils.escape(grp, butor.sec.group.fte);
				this._groups[grp.id] = grp.description;
				sb.append("<option value=\"" +grp.id +"\">" +grp.description +"</option>");
			}
		}, this));

		this._jqe.find('#resultTable').tablesorter({textExtraction : 
			function(node) {
				var val = node.textContent;
				if (val == '') {
					val = node.innerHTML;
				}
				return val;
			}
		});

		var listAuthsP = $.proxy(this._listAuths, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			var dlg = butor.sec.auth.AuthDlg.view(this._tableCtxtMenu.getHoveredItem().authId);
			if (dlg) {
				dlg.bind('auth-updated', listAuthsP);
			}
		}, this));
		if (this._canUpdate) {
			this._ctxMenu.find('li.edit').show().find('a').click($.proxy(function(e_) {
				var dlg = butor.sec.auth.AuthDlg.edit(this._tableCtxtMenu.getHoveredItem().authId);
				if (dlg) {
					dlg.bind('auth-updated', listAuthsP);
				}
			}, this));
			this._ctxMenu.find('li.delete').show().find('a').click($.proxy(function(e_) {
				var rReqId = butor.sec.auth.AuthAjax.deleteAuth(this._tableCtxtMenu.getHoveredItem(), $.proxy(function(result){
					if (!AJAX.isSuccessWithData(result, this._rReqId)) {
						return;
					}
					// refresh
					this._listAuths();
				}, this));
			}, this));
			this._jqe.find('#addBtn').show().click($.proxy(function(){
				var dlg = butor.sec.auth.AuthDlg.newAuth();
				if (dlg) {
					dlg.bind('auth-updated', listAuthsP);
				}
			}, this));
		}
		
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._jqe.find('#resultTable'), this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			if (butor.Utils.isEmpty(e.data.revNo)) {
				this._ctxMenu.find('li.view').hide();
				this._ctxMenu.find('li.edit').hide();
			} else {
				this._ctxMenu.find('li.view').show();
				if (this._canUpdate) {
					this._ctxMenu.find('li.edit').show();
				}
			}
		}, this));

		this._jqe.find('#goBtn').click(listAuthsP);
		this._jqe.find('#resetBtn').click($.proxy(this._reset, this));
		
		this._jqe.find('#sysSB').change($.proxy(function(){
			var sys = this._jqe.find('#sysSB').val();
			if (!butor.Utils.isEmpty(sys)) {
				this._jqe.find('#roleSB').val('');
			}
			this._listFuncs();
		}, this));
		this._jqe.find('#roleSB').change($.proxy(function(){
			var role = this._jqe.find('#roleSB').val();
			if (!butor.Utils.isEmpty(role)) {
				this._jqe.find('#sysSB').val('').change();
			}
		}, this));
		this._jqe.find('#groupSB').change($.proxy(function(){
			var grp = this._jqe.find('#groupSB').val();
			if (!butor.Utils.isEmpty(grp)) {
				this._userLookup.clear();
			}
		}, this));

		this._userLookup = new butor.sec.user.UserLookup(this._jqe.find('#member'));
		this._userLookup.bind('user-selected', $.proxy(this._criteriaChanged, this));

		this._jqe.find(".form").keypress($.proxy(function(e) {
			if (e.keyCode == '13') {
				this._listAuths();
				return false;
			}
		}, this));
		this._jqe.find("#resultTable").delegate('a.group-link', 'click', function(e_) {
			butor.sec.group.GroupDlg.view($(this).attr('groupId'));
		});
		this._jqe.find("#resultTable").delegate('a.role-link', 'click', function(e_) {
			butor.sec.role.RoleDlg.view($(this).attr('roleId'));			
		});
		this._jqe.find("#resultTable").delegate('tr.auth', 'dblclick', function(e_) {
			var dlg = butor.sec.auth.AuthDlg.view($(this).attr('authId'));
			if (dlg) {
				dlg.bind('auth-updated', listAuthsP);
			}
		});
		var pArgs = App.getPageArgs();
		if (pArgs && pArgs.member) {
			this._userLookup.select(pArgs.member);
			this._listAuths();
		}
	},
	_criteriaChanged : function() {
		this.clear();
	},
	_reset : function() {
		this._jqe.find('#sysSB').val('');
		this._jqe.find('#funcSB').empty('');
		this._jqe.find('#roleSB').val('');
		this._jqe.find('#groupSB').val('');
		this._userLookup.clear();
		this.clear();
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		this._jqe.find('#resultTable > tbody').empty();
	},
	_listFuncs : function() {
		App.hideMsg();
		var sb = this._jqe.find('#funcSB');
		sb.empty();
		sb.append("<option value=\"\"></option>");
		var sys = this._jqe.find('#sysSB').val();
		if (sys == '')
			return;

		App.mask(this.tr('Working'));
		this._fReqId = butor.sec.func.FuncAjax.listFunc(sys, "auths", $.proxy(function(result) {
			App.unmask();
			if (!AJAX.isSuccessWithData(result, this._fReqId)) {
				return;
			}
			var data = result.data;
			for (var ii=0; ii<data.length; ii++) {
				var item = data[ii];
				sb.append("<option value=\"" +item.func +"\">" +item.description +"</option>");
			}
		}, this));
	},
	_listAuths : function() {
		App.hideMsg();
		this.clear();
		var crit = {withData:null};
		var val = this._jqe.find('#sysSB').val();
		if (!butor.Utils.isEmpty(val)) {
			crit.sys = val;
		}
		var val = this._jqe.find('#funcSB').val();
		if (!butor.Utils.isEmpty(val)) {
			crit.whatTypes = ["func"];
			crit.whats = [val];
		}
		val = this._jqe.find('#roleSB').val();
		if (!butor.Utils.isEmpty(val)) {
			crit.whatTypes = ["role"];
			crit.whats = [val];
		}

		var user = this._userLookup.getSelection();
		if (user) {
			crit.whoTypes = ["user"];
			crit.whos = [user.id];
		}
		val = this._jqe.find('#groupSB').val();
		if (!butor.Utils.isEmpty(val)) {
			crit.whoTypes = ["group"];
			crit.whos = [val];
		}
	
		App.mask(this.tr('Working'));
		this._fReqId = butor.sec.auth.AuthAjax.listAuth(crit, 
			$.proxy(function(result) {
				App.unmask();
				if (!AJAX.isSuccessWithData(result, this._fReqId)) {
					return;
				}
				var data = result.data;
				var tbody = this._jqe.find('#resultTable > tbody');
				for (var ii=0; ii<data.length; ii++) {
					var auth = data[ii];
					
					butor.Utils.escape(auth, butor.sec.auth.fte);
					
					var row = $("<tr class=\"auth\" authId=\"" +auth.authId +"\" revNo=\"" +auth.revNo +"\">");
					var whoLink;
					if (auth.whoType === 'group') {
						var grp = this._groups[auth.who];
						if (!grp) {
							grp = auth.who;
						}
						whoLink = '<a class="group-link" groupId="' +auth.who +'">' +grp +'</a>';
					} else {
						whoLink = auth.who;
					}
					var roleLink;
					if (auth.whatType === 'role') {
						var role = this._roles[auth.what];
						if (!role) {
							role = auth.what;
						}
						whatLink = '<a class="role-link" roleId="' +auth.what +'">' +role +'</a>';
					} else {
						whatLink = auth.what;
					}
					row.html("<td class=\"number\">" +(ii +1) +"</td>" +
							'<td class="sec-auth-who">' +whoLink +'</td><td class="sec-auth-who">' +this._whoTypeCS.get(auth.whoType) +"</td>" +
							'<td class="sec-auth-what">' +whatLink +'</td><td class="sec-auth-what">' +this._whatTypeCs.get(auth.whatType) +"</td>" +
							'<td class="sec-auth-what">' +(auth.sys ? this._sysCs.get(auth.sys) : '') +"</td><td>" +(this._accessModeCs.get(auth.mode) || '') +"</td>" +
							'<td class="sec-auth-on-data align-center">' +(auth.dataId > -1 ? '<i class="fa fa-check"></i>' : "") +"</td>" +
							"<td>" +this.formatDate(auth.startDate) +"</td><td>" +this.formatDate(auth.endDate) +"</td>" +
							"<td>" +this.formatDateTime(auth.stamp) +"</td><td>" +auth.userId +"</td>");

					tbody.append(row);
				}
				this._jqe.find("#resultTable").trigger("update");
			}, this));
	}
});

butor.sec.auth.AuthDlg = butor.sec.auth.AuthDlg || butor.dlg.Dialog.define({
	statics : {
		newAuth : function(user) {
			var args = {'user':user, 'mode':'add'};
			return butor.sec.auth.AuthDlg.show(args);
		},
		view : function(id) {
			var args = {'id':id, 'mode':'view'};
			return butor.sec.auth.AuthDlg.show(args);
		},
		edit : function(id) {
			var args = {'id':id, 'mode':'edit'};
			return butor.sec.auth.AuthDlg.show(args);
		},
		show : function(args) {
			dlgOpts = {
				title : butor.sec.tr("Authorisation"),
				width : 900,
				height : 500,
				resizable: true,
				maximizable: true,
				bundleId: 'sec'
			};
			args = args || {};

			var dlg = new butor.sec.auth.AuthDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_auth : null,
	_reqId : null,
	_isEdit : false,
	_switchedToUpdate : false,
	_sysCs : null,
	_iReqId : null,
	_grpReqId : null,
	_usrReqId : null,
	_accessModeCs : null,
	_authDataPane : {},
	_authData : {},
	_userFilter : null,
	_startDate : null,
	_endDate : null,
	_canUpdate : App.hasAccess('sec', 'auths', butor.sec.ACCESS_MODE_WRITE),
	tr : butor.sec.tr,
	parseDate : butor.Utils.parseDate,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/bsec/authDlg.html');
	},
	_init : function() {
		this.base();
		App.translateElem(this._jqe, 'sec');

		var menu = this._jqe.find('#tabAddDataBundle ul.dropdown-menu');
		menu.empty();
		var dts = butor.sec.auth.getDataEditorNames();
		for (var dt in dts) {
			$('<li><a alt="' +dt +'"><span class="bundle">' +this.tr(dts[dt]) +'</span></a></li>').appendTo(menu);
		}

		var task1 = $.proxy(function() {
			var dfr =  $.Deferred();
			this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'auths',
				$.proxy(function(result) {
					if (!AJAX.isSuccessWithData(result, this._sReqId)) {
						dfr.reject();
						return;
					}
					App.Utils.fillSelect(this._jqe.find('#sysSB'), result.data, {showEmpty:false});
					dfr.resolve();
				}, this));
			return dfr.promise();
		}, this);
		
		var task2 = $.proxy(function() {
			var dfr =  $.Deferred();
			this._iReqId = App.AttrSet.getCodeSets([{id:"auth-who-type", lang:App.getLang()},
					{id:"auth-what-type", lang:App.getLang()},
					{id:"auth-mode", lang:App.getLang()}],
				$.proxy(function(result) {
					if (!result || result.hasError || !result.data || result.reqId != this._iReqId) {
						dfr.reject();
						return;
					}
					butor.Utils.fillSelect(this._jqe.find('#whoTypeSB'), result.data[0], {showEmpty:true});
					butor.Utils.fillSelect(this._jqe.find('#whatTypeSB'), result.data[1], {showEmpty:true});
					this._accessModeCs = result.data[2];
					butor.Utils.fillSelect(this._jqe.find('#modeSB'), this._accessModeCs, {showEmpty:false});
	
					this._jqe.find('#whoTypeSB').val('group').change();
					this._jqe.find('#whatTypeSB').val('role').change();
					dfr.resolve();
				}, this));
			return dfr.promise();
		}, this);
		
		$.when(task1(), task2()).always($.proxy(function() {
			App.unmask();
			switch (this._args.mode) {
			case 'add':
				this._newAuth();
				break;
			case 'view':
				this._view(this._args.id);
				break;
			case 'edit':
				this._edit(this._args.id);
				break;
			}
		}, this)).fail(function() {
			//ok
		});

		this._startDate = new butor.DatePicker(this._jqe.find('.startDateGroup'),
				{'errorMsg' : this.tr('Start date is not valid!'), 'msgPanel' : this.getMsgPanel()});
		this._endDate = new butor.DatePicker(this._jqe.find('.endDateGroup'),
				{'errorMsg' : this.tr('End date is not valid!'), 'msgPanel' : this.getMsgPanel()});

		var self = this;
		this._jqe.find('#tabAddDataBundle a').click(function() {
			var dataType = $(this).attr('alt');
			self._addNewDataPane(dataType);
		});
		this._jqe.find('#authTabs li').click(function(e_) {
			var tabId = $(this).attr("id");
			//self._jqe.find('#authPanel').hide();
			if (tabId === 'tabAuth') {
				self._jqe.find('#authDataPanes').hide();
				self._jqe.find('#tabAddDataBundle').hide();
				self._jqe.find('#authPanel').addClass('active').show();
			} else if (tabId === 'tabAuthData') {
				self._jqe.find('#authPanel').hide();
				self._jqe.find('#tabAddDataBundle').show();
				self._jqe.find('#authDataPanes').addClass('active').show();
				self._resized();
			} else {
				return;
			}
			App.Utils.selectNavItem("#authTabs", "#" +tabId);
		});

		this._jqe.find('#whoTypeSB').change($.proxy(this._whoTypeChanged, this));
		this._jqe.find('#whatTypeSB').change($.proxy(this._whatTypeChanged, this));
		this._jqe.find('#sysSB').change($.proxy(this._whatTypeChanged, this));
		
		this._userLookup = new butor.sec.user.UserLookup(this._jqe.find('#userFilter'), 'auths');
		this._userLookup.bind('user-selected', $.proxy(this._whoTypeChanged, this));
		
		this._jqe.find('#cancelBtn').click(function() {
			if (self._switchedToUpdate) {
				self._rollback();
			} else {
				self.close();
			}
		});
		this._jqe.find('#closeBtn').click(function() {
			self.close();
		});

		this._jqe.find('#saveBtn').click(function() {
			self._save();
		});
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').click(function() {
				self._switchedToUpdate = true;
				self._editMode();
			});
		}
		this.bind('resized', $.proxy(this._resized, this));
	},
	_resized : function() {
		var top = this._jqe.find('#dataTabs').height();
		this._jqe.find('div[id|="dataPane"]').css('top', top);
	},
	_rollback : function() {
		this._switchedToUpdate = false;
		this._isEdit = false;
		this._view(this._auth.authId);
	},
	_addNewDataPane : function(dataType) {
		var dataPaneId = 'dataPane-' +dataType;
		var dataPane = this._jqe.find('#' +dataPaneId);
		if (dataPane.length > 0) {
			return;
		}
		var caption = butor.sec.auth.getDataEditorNames()[dataType];
		var tab = $('<li id="tab-' +dataType +'" alt="' +dataType +'"><a>'+
			'<i class="fa fa-filter"></i> <span class="bundle">' +this.tr(caption) +'</span></a></li>');
		this._jqe.find('#dataTabs').append(tab);
		var self = this;
		tab.click(function() {
			self._dataPaneSelected($(this));
		});
		tab.click();
	},
	_dataPaneSelected : function(tab) {
		var dataType = tab.attr("alt");
		if (!dataType) {
			return;
		}
		this._jqe.find('div[id|="dataPane"]').hide();
		var dataPaneId = 'dataPane-' +dataType;
		if (this._jqe.find('#' +dataPaneId).length == 0) {
			var dataPane = this._jqe.find('#templateDataPane').clone(true);
			dataPane.attr('id', dataPaneId);
			this._jqe.find('#authDataPanes').append(dataPane);
			this._resized();
			dataPane.show();
			this.mask(this.tr('Loading'));
			var cmp = butor.sec.auth.createDataEditor({'elem':dataPane, 
				'dataType' : dataType,
				'msgPanel':this.getMsgPanel()});
			if (cmp) {
				cmp.bind('ready', $.proxy(function(e) {
					//App.translateElem(dataPane, 'sec');
					//App.Utils.fillSelect(dataPane.find('[name="modeSB"]'), this._accessModeCs, {showEmpty:false});
					this.unmask();
					cmp.setData(this._authData[dataType]);
					if (this._isEdit) {
						cmp.enable();
					} else {
						cmp.disable();
					}
				},this));
				this._authDataPane[dataType] = cmp;
			}
		} else {
			this._jqe.find('#' +dataPaneId).show();
		}
		App.Utils.selectNavItem("#dataTabs", "#tab-" +dataType);
	},
	_whoTypeChanged : function() {
		var groupSB = this._jqe.find('#groupSB');
		groupSB.empty();
		var wt = this._jqe.find('#whoTypeSB').val();
		if (wt === 'group') {
			this._jqe.find('#userFilter').hide();
			groupSB.show();
			this._listGroup($.proxy(function(list) {
				App.Utils.fillSelect(groupSB, list, 
					{showEmpty:true});
				if (this._auth) {
					groupSB.val(this._auth.who);
				}
			}, this));
		} else if (wt === 'user') {
			groupSB.hide();
			this._jqe.find('#userFilter').show();
		}
	},
	_whatTypeChanged : function() {
		var whatSB = this._jqe.find('#whatSB');
		whatSB.empty();
		var wt = this._jqe.find('#whatTypeSB').val();
		if (wt === 'role') {
			this._jqe.find('#modeBlock').hide();
			this._jqe.find('#sysTD').hide();
			this._jqe.find('#sysSB').val('');
			this._listRole($.proxy(function(list) {
				App.Utils.fillSelect(whatSB, list, {showEmpty:true,
					idFN:'id', descFN:'description'});
				if (this._auth) {
					whatSB.val(this._auth.what);
				}
			}, this));
		} else if (wt === 'func') {
			this._jqe.find('#modeBlock').show();
			this._jqe.find('#sysTD').show();
			this._listFunc($.proxy(function(list) {
				App.Utils.fillSelect(whatSB, list, {showEmpty:false,
					idFN:'func', descFN:'description'});
				if (this._auth) {
					whatSB.val(this._auth.what);
				}
			}, this));
		}
	},
	_listGroup : function(handler) {
		this._grpReqId = butor.sec.group.GroupAjax.listGroup(null, "auths", {msgHandler:this.getMsgPanel(), 
			'scope':this, callBack:function(result) {
			if (!AJAX.isSuccessWithData(result, this._grpReqId)) {
				return;
			}

			handler(result.data);
		}});
	},
	_listRole : function(handler) {
		this._usrReqId = butor.sec.role.RoleAjax.listRole(null, "auths", {msgHandler:this.getMsgPanel(), 
			'scope':this, callBack:function(result) {
			if (!AJAX.isSuccessWithData(result, this._usrReqId)) {
				return;
			}

			handler(result.data);
		}});
	},
	_listFunc : function(handler) {
		var sys = this._jqe.find('#sysSB').val();
		if (sys == '')
			return;

		this._iReqId = butor.sec.func.FuncAjax.listFunc(sys, "auths", {msgHandler:this.getMsgPanel(), 
			'scope':this, callBack:function(result) {
			if (!AJAX.isSuccessWithData(result, this._iReqId)) {
				return;
			}

			handler(result.data);
		}});
	},
	_editMode : function() {
		this._isEdit = true;
		this._jqe.find('#authPanel .editable').removeAttr('disabled');
		this._jqe.find('#tabAddDataBundle .editable').removeAttr('disabled');
		for (dataType in this._authDataPane) {
			this._authDataPane[dataType].enable();
		}
		this._jqe.find('#closeBtn').hide();
		this._jqe.find('#modifyLink').hide();
		this._jqe.find('#cancelBtn').show().removeClass('hidden');
		this._jqe.find('#saveBtn').show().removeClass('hidden');
		this.hideMsg();
	},
	_disableForm : function() {
		this._jqe.find('#authPanel .editable').attr('disabled', 'disabled');
		this._jqe.find('#tabAddDataBundle .editable').attr('disabled', 'disabled');
		for (dataType in this._authDataPane) {
			this._authDataPane[dataType].disable();
		}
		this._jqe.find('#funcsSB').empty();
		this._jqe.find("#saveBtn").hide();
		this._jqe.find("#cancelBtn").hide();
		this._jqe.find('#closeBtn').show();
		if (this._canUpdate) {
			this._jqe.find('#modifyLink').show();
		}
	},
	_save : function() {
		var args = this._getData();
		if (butor.Utils.isEmpty(args.whoType) || butor.Utils.isEmpty(args.who)) {
			this.msgError(this.tr('Please fill "Authorize" part'));
			return;
		}
		if (butor.Utils.isEmpty(args.whatType) || butor.Utils.isEmpty(args.what)) {
			this.msgError(this.tr('Please fill "On" part'));
			return;
		}

		if (args.whatType === 'func' && butor.Utils.isEmpty(args.mode)) {
			this.msgError(this.tr('Please select access mode'));
			return;
		}
		this.mask(this.tr("Saving"));
		this.hideMsg();
		this._reqId = butor.sec.auth.AuthAjax.updateAuth(args, {msgHandler:this.getMsgPanel(), 
			'scope':this, callBack:function(result) {
				this.unmask();
				if (!result || result.hasError || !result.data|| 
						result.data.length == 0 || result.reqId != this._reqId) {
					return;
				}
				this._auth = args;
				this.getMsgPanel().showMsg(this.tr('Saved'));
				var ak = result.data[0];
				this.fire('auth-updated', ak.authId);
				this._auth.authId = ak.authId;
				this._auth.revNo = ak.revNo;
				this._disableForm();
			}
		});
	},
	_getData : function() {
		var auth = {'authId' : this._auth.authId, 
			'revNo' : this._auth.revNo};

		auth.description = this._jqe.find("#description").val();

		var val = this._startDate.getDate();
		if (val) {
			// time is 00:00:00
			val.setHours(0, 0, 0);
			auth.startDate = butor.Utils.formatDateTime(val);
		}
		val = this._endDate.getDate();
		if (val) {
			// set end time to 23:59:59
			val.setHours(23, 59, 59);
			auth.endDate = butor.Utils.formatDateTime(val);
		}

		auth.whoType = this._jqe.find('#whoTypeSB').val();
		if (auth.whoType === 'user') {
			var user = this._userLookup.getSelection();
			if (user) {
				auth.who = user.id;
			}
		} else {
			auth.who = this._jqe.find('#groupSB').val();
		}

		auth.whatType = this._jqe.find('#whatTypeSB').val();
		auth.what = this._jqe.find('#whatSB').val();
		if (auth.whatType === 'func') {
			auth.sys = this._jqe.find('#sysSB').val();
			auth.mode = this._jqe.find('#modeSB').val();
		} else {
			auth.sys = '';
			auth.mode = 0;
		}

		var data = [];
		for (dataType in this._authDataPane) {
			var pd = this._authDataPane[dataType].getData();
			if (pd) {
				for (var i=0,c=pd.length; i<c; i+=1) {
					data.push(pd[i]);
				}
			}
		}
		auth.data = data;
		return auth;
	},
	_newAuth : function() {
		this._auth = {authId:null, revNo:null};
		this._editMode();
	},
	_view : function(id, isEdit) {
		this._isEdit = isEdit;
		this._disableForm();
		this.hideMsg();
		this.mask(this.tr("Initializing"));
		this._reqId = butor.sec.auth.AuthAjax.readAuth(id, {msgHandler:this.getMsgPanel(), 
			'scope':this, callBack:function(result) {
				this.unmask();
				if (!AJAX.isSuccessWithData(result, this._reqId)) {
					return;
				}
				this._auth = result.data[0];
				this._showAuthInfo();
				if (this._isEdit) {
					this._editMode();
				}
		}});
	},
	_edit : function(id) {
		this._view(id, true);
	},
	_clear : function() {
		this._jqe.find('#funcsSB').empty();
		this._jqe.find('#sysSB').val('');
		this._jqe.find('#modeSB').val('');
		this._startDate.setDate('');
		this._endDate.setDate();
		this._jqe.find('#dataTabs li[id|="tab"]').remove();
		this._jqe.find('#authDataPanes div[id|="dataPane"]').remove();
		this._authDataPane = {};
	},
	_showAuthInfo : function() {
		this._clear();
		if (!this._auth) {
			return;
		}
		this._jqe.find('#whoTypeSB').val(this._auth.whoType).change();
		if (this._auth.whoType === 'user') {
			this._userLookup.select(this._auth.who);
		}
		this._jqe.find('#whatTypeSB').val(this._auth.whatType).change();
		if (this._auth.whatType === 'func') {
			this._jqe.find('#sysSB').val(this._auth.sys).change();
		}

		if (!butor.Utils.isEmpty(this._auth.startDate)) {
			this._startDate.setDate(butor.Utils.parseDate(this._auth.startDate));
		}
		if (!butor.Utils.isEmpty(this._auth.endDate)) {
			this._endDate.setDate(butor.Utils.parseDate(this._auth.endDate));
		}
		this._jqe.find('#modeSB').val(this._auth.mode);
		var items = this._auth.data;
		this._authData = {};
		if (items) {
			for (var i=0,c=items.length; i<c; i+=1) {
				var item = items[i];
				var list = this._authData[item.dataType];
				if (!list) {
					list = [];
					this._authData[item.dataType] = list;
				}
				list.push(item);
			}
		}
		for (dt in this._authData) {
			this._addNewDataPane(dt);
		}
	}
});

butor.sec.auth.AuthAjax = butor.sec.auth.AuthAjax || function() {
	return {
		listAuth: function(member, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:false, service:'listAuth'},
				[member], handler);
		},
		readAuth: function(id_, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:false, service:'readAuth'},
				[id_], handler);
		},
		updateAuth: function(args, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:false, service:(!butor.Utils.isEmpty(args.authId) ? 'updateAuth' : 'createAuth')},
				[args], handler);
		},
		deleteAuth: function(authKey, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:false, service:'deleteAuth'},
				[authKey], handler);
		},
		listAuthSys: function(sys, func, handler) {
			return AJAX.call('/bsec/auth.ajax', 
				{streaming:false, service:'listAuthSys'},
				[sys, func], handler);
		}
	};
}();

butor.sec.user.AuthDataUserEditor = butor.sec.user.AuthDataUserEditor || butor.Class.define({
	_jqe : null,
	_showAllOption : false,
	_selection : null,
	_reqId: null,
	_isEdit : false,
	_selectionMap : {},
	_msgPanel : null,
	_enabled : false,
	_cReqId : null,
	_allCBLbl : null,
	_allCB : null,
	_selectionSB : null,
	_isReady : false,
	tr : butor.sec.tr,
	
	construct : function(args) {
		this._jqe = args.elem;
		this._jqe.load("/bsec/authDataUserEditor.html", null, $.proxy(function() {
			this._showAllOption = (args.showAllOption === undefined || args.showAllOption);
			this._msgPanel = args.msgPanel;
			this._allCBLbl = this._jqe.find('#adue-allCB');
			this._allCB = this._allCBLbl.find('input')
			this._selectionSB = this._jqe.find('#selectionSB');
	
			this._userLookup = new butor.sec.user.UserLookup(this._jqe.find('#userFilter'));

			App.translateElem(this._jqe, 'sec');
	
			if (this._showAllOption) {
				this._allCBLbl.show();
				this._allCB.change($.proxy(this._allSelectionChanged, this));
			} else {
				this._allCBLbl.hide();
			}
		
			this._jqe.find('#removeBtn').click($.proxy(this._removeSelectedItems, this)).attr('title', this.tr('Remove'));
			this._jqe.find('#addBtn').click($.proxy(this._addSelectedItems, this)).attr('title', this.tr('Add'));
			this.enable();
			this._isReady = true;
			this.fire('ready', null);
		}, this));
	},
	_allSelectionChanged : function() {
		if (this._allCB.is(":checked")) {
			this._jqe.find('.adue-editable').butor('disable');
			if (this._enabled) {
				this._allCB.butor('enable');
			}
			this._selectionSB.empty().butor('disable');
			this._userLookup.clear();
			this._selectionMap = {};
		} else {
			this._jqe.find('.adue-editable').butor('enable');
			this._selectionSB.butor('enable');
		}
	},
	enable : function() {
		this._enabled = true;
		if (!this._isReady) {
			return;
		}
		this._jqe.find('.adue-editable').butor('enable');
		this._selectionSB.butor('enable');
		this._allSelectionChanged(); //set enability depending on checkbox
	},
	disable : function() {
		this._enabled = false;
		this._jqe.find('.adue-editable').butor('disable');
		this._userLookup.clear();
	},
	_addSelectedItems : function() {
		var self = this;
		var user = this._userLookup.getSelection();
		if (user) {
			//var opt = $('<option value="' +user.id +'">' +user.displayName +'</option>');
			var opt = $('<div class="checkbox"><label> <input value="' +user.id +'" type="checkbox">' +user.displayName +'</label></div>');
			this._selectionSB.append(opt);
			this._selectionMap[user.id] = user.id;
			this._userLookup.clear();
		}
	},
	_removeSelectedItems : function() {
		var self = this;
		this._jqe.find('#selectionSB input:checked').each(function(index, elem) {
			var item = $(this);
			var member = item.attr('value');
			item.parent().parent().remove();
			delete self._selectionMap[member];
		});
	},
	_close : function() {
		this._parent.close();
	},
	_mask : function(msg, to) {
		this._jqe.mask(msg, to);
	},
	_unmask : function() {
		this._jqe.unmask();
	},
	getUsers : function() {
		var data = [];
		if (this._showAllOption && this._allCB.is(':checked')) {
			data.push('*');
			return data;
		}
		for (key in this._selectionMap) {
			data.push(key);
		}
		return data;
	},
	setUsers : function(selection) {
		this._selection = selection;
		this._userLookup.clear();
		this._selectionMap = {};
		if (!this._selection) {
			return;
		}
	
		this._selectionSB.empty();
		for (var ii=0; ii<this._selection.length; ii++) {
			var item = this._selection[ii];
			if (item == '*') {
				this._allCB.attr('checked', true).change();
				return;
			}
			//var opt = $("<option value=\"" +item +"\">" +item +"</option>");
			var opt = $('<div class="checkbox"><label> <input value="' +item +'" type="checkbox">' +item +'</label></div>');
			this._selectionSB.append(opt);
			this._selectionMap[item] = item;
		}
	},
	getData : function() {
		var users = this.getUsers();
		var data = [];
		for (var i=0,c=users.length; i<c; i+=1) {
			data.push({'sys':'sec','dataType':'user','d1':users[i]});
		}
		return data;
	},
	setData : function(data) {
		var users = [];
		if (data) {
			for (var i=0,c=data.length; i<c; i+=1) {
				users.push(data[i].d1);
			}
		}
		this.setUsers(users);
	}
});
if (!butor.sec.auth.getDataEditorNames()['user']) {
	butor.sec.auth.registerFactory('user', 'User', function(args) {
		return new butor.sec.user.AuthDataUserEditor(args);
	});
}

//# sourceURL=butor.sec.js
